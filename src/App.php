<?php

namespace WebuddhaInc\FormGimp;

use \WebuddhaInc\Params;

class App {

  public $db;
  public $config;
  public $request;
  public $router;
  public $lang;

  public function __construct(DB $db, Config $config, Request $request) {

    defined('FORMGIMP') or define('FORMGIMP', 1);

    // Session Start
    if (
      !headers_sent() &&
      php_sapi_name() !== 'cli' &&
      (version_compare(phpversion(), '5.4.0', '>=') ? session_status() !== PHP_SESSION_ACTIVE : session_id() === '')
      )
      session_start();

    // Attach
    Common::$app   = $this;

    // Stage
    $this->db      = $db;
    $this->config  = new Config(array(
      'db_prefix'      => null,
      'web_root'       => null,
      'app_path'       => dirname(__FILE__),
      'route_root'     => null,
      'route_callback' => null,
      'media_callback' => null,
      'media_path'     => dirname(__FILE__) . '/media',
      'media_root'     => null,
      'theme_path'     => null,
      'theme_root'     => null,
      'lang_locale'    => 'en_US',
      'lang_callback'  => null
      ), $config);
    $this->request = $request;
    if( $this->config->get('fg_auto_archive') )
      Common::archiveSubmissions();
    $this->router  = new Router($this);

    // Installer
    $installer = new Installer($this);
    $installer->checkSchema();

    // Language
    $this->lang = new Lang($this);
    $this->lang->loadLanguage( $this->config->get('lang_locale') );
    if (!empty($this->config->get('lang_callback')))
      $this->lang->setCallback($this->config->get('lang_callback'));

    // Legacy
    $GLOBALS['gimpDB'] = $this->db;
    $GLOBALS['fg_coreFormFields'] = Common::$coreFormFields;
    Common::loadSetupGlobals();

  }

  public function renderMsg( $type, $message ){
    return '<div class="formgimp message '.$type.'">' . $message . '</div>';
  }

  public function route($route){
    return $this->router->route($route);
  }

  public function redirect($route, $message=null){
    $url = $this->route($route);
    $_SESSION['formgimp_message'] = $message;
    if (defined('FORMGIMP_DEBUG') && FORMGIMP_DEBUG) {
      inspect( '<a href=' . $url . '>' . $url . '</a>', $message );die(__LINE__.': '.__FILE__);
    }
    if (headers_sent()) {
      echo '<script>document.location=\''.$url.'\';</script>';
    }
    else {
      header('Location: ' . $url);
    }
    exit;
  }

  public function appPath(){
    return __DIR__;
  }

  public function webRoot(){
    $url = parse_url($this->config->get('web_root'));
    return $url['scheme'] . '://'. $url['host'] . $url['path'];
  }

  public function attachPath(){
    return $this->config->get('attach_path', $this->mediaPath().'/attachments');
  }

  public function mediaPath(){
    return $this->config->get('media_path', $this->config->get('app_path').'/media');
  }

  public function mediaRoot(){
    $url = parse_url($this->config->get('media_root', $this->config->get('web_root').'/media'));
    return $url['scheme'] . '://'. $url['host'] . $url['path'];
  }

  public function mediaLookup( $type, $file ){
    $media_path   = Common::$app->mediaPath();
    $media_root   = Common::$app->mediaRoot();
    $theme_path   = Common::$app->config->get('theme_path');
    $theme_root   = Common::$app->config->get('theme_root');
    $theme_ready  = $theme_path && $theme_root && is_dir($theme_path);
    $lookup_paths = array();
    switch ($type) {
      case 'js':
        $lookup_paths = array('js', 'forms');
        break;
      case 'css':
        $lookup_paths = array('css', 'forms');
        break;
      case 'form':
        $lookup_paths = array('forms');
        break;
      case 'email':
        $lookup_paths = array('emails');
        break;
      case 'processor':
        $lookup_paths = array('form_processors');
        break;
    }
    $lookup_paths[] = '';
    foreach( $lookup_paths AS $lookup_path ){
      if ($theme_ready && is_readable($theme_path . '/formgimp/' . ($lookup_path ? $lookup_path . '/' : '') . $file)) {
        return (object)array(
          'path' => $theme_path . '/formgimp/' . ($lookup_path ? $lookup_path . '/' : '') . $file,
          'url'  => $theme_root . '/formgimp/' . ($lookup_path ? $lookup_path . '/' : '') . $file
          );
      }
      if (is_readable($media_path . '/' . ($lookup_path ? $lookup_path . '/' : '') . $file)) {
        return (object)array(
          'path' => $media_path . '/' . ($lookup_path ? $lookup_path . '/' : '') . $file,
          'url'  => $media_root . '/' . ($lookup_path ? $lookup_path . '/' : '') . $file
          );
      }
    }
  }

  public function mediaInclude( $type, $file, $params = array() ){
    if ($this->config->get('media_callback') && is_callable($this->config->get('media_callback'))){
      $res = call_user_func_array($this->config->get('media_callback'), array( $type, $file, new Params($params) ));
      if ($res !== false)
        return $res;
    }
    $url = preg_match('/^([a-z]+\:\/\/|\/)/',$file) ? $file : $this->mediaRoot() . '/' . $file;
    switch ($type) {
      case 'img':
        echo '<img src="'. $url .'" />';
        break;
      case 'css':
        echo '<link href="'. $url .'" rel="stylesheet" type="text/css" />';
        break;
      case 'js':
        echo '<script language="javascript" src="'. $url .'" type="text/javascript"></script>';
        break;
    }
  }

  /**
   * [render description]
   * @param  [type] $task [description]
   * @return [type]       [description]
   */
  function render($task) {
    $output = array();
    if (@$_SESSION['formgimp_message']) {
      $output[] = $this->renderMsg('alert', $_SESSION['formgimp_message']);
      unset($_SESSION['formgimp_message']);
    }
    $output[] = (new Controller($this))->render($task);
    return implode("\n", $output);
  }

  /*
  function loadForm($form_name='contact') {
    $wb_params = $this->params;
    $wb_params->set( 'contact', $form_name );
    $wb_params->set('lib_path', plugin_dir_path( __FILE__  ));
    $formgimp_lib_path = $wb_params->get('lib_path');
    $page = $formgimp_lib_path."formgimp/client/renderForm.php";
    include($page);
    return $form;
  }
  */

  /*
  function submitForm($form_name=null) {
    $wb_params = $this->params;
    $wb_params->set( 'form', $form_name );
    $wb_params->set('lib_path', plugin_dir_path( __FILE__  ));
    $formgimp_lib_path = $wb_params->get('lib_path');
    $page = $formgimp_lib_path."formgimp/client/formgimp.php";
    include($page);
    return $chicken;
  }
  */

}
