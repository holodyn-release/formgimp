<?php

namespace WebuddhaInc\FormGimp;

class Request {

  public $query;
  public $request;
  public $attributes;
  public $cookies;
  public $files;
  public $server;
  public $content;

  /**
   * [__construct description]
   * @param array       $query      [description]
   * @param array       $request    [description]
   * @param array       $attributes [description]
   * @param array       $cookies    [description]
   * @param array       $files      [description]
   * @param array       $server     [description]
   * @param string|null $content    [description]
   */
  public function __construct(
    array $query      = array(),
    array $request    = array(),
    array $attributes = array(),
    array $cookies    = array(),
    array $files      = array(),
    array $server     = array(),
    string $content   = null
    ) {
    $this->query      = new RequestObject($query);
    $this->request    = new RequestObject($request);
    $this->attributes = new RequestObject($attributes);
    $this->cookies    = new RequestObject($cookies);
    $this->files      = new RequestObject($files);
    $this->server     = new RequestObject($server);
    $this->content    = $content;
  }

  /**
   * [createFromGlobals description]
   * @return [type] [description]
   */
  public static function createFromGlobals(){
    return new self(
      $_GET,
      $_REQUEST,
      array(),
      $_COOKIE,
      $_FILES,
      $_SERVER,
      null
      );
  }

  /**
   * [getUserStateFromRequest description]
   * @param  [type] $sessionKey   [description]
   * @param  [type] $requestKey   [description]
   * @param  [type] $defaultValue [description]
   * @return [type]               [description]
   */
  public function getSessionRequest( $sessionKey, $requestKey, $defaultValue = null, $filter = null ) {
    if (is_null($this->request->get($requestKey)) && isset($_SESSION[$sessionKey])) {
      $value = @$_SESSION[$sessionKey];
    }
    else {
      $_SESSION[$sessionKey] = $this->request->get($requestKey, $defaultValue, $filter);
      $value = $_SESSION[$sessionKey];
    }
    /* Filter Logic / Callback */
    return $value;
  }

}