<?php

namespace WebuddhaInc\FormGimp;

class Lang {

  public $app;
  private $lang;
  private $callback;

  public function __construct(App $app){
    $this->app = $app;
  }

  public function loadLanguage($locale){
    if (file_exists(__DIR__ . '/lang/' . $locale . '.ini'))
      $this->lang = parse_ini_file(__DIR__ . '/lang/' . $locale . '.ini');
  }

  public function setCallback($callback){
    $this->callback = $callback;
  }

  public function get($string, $params=null){
    $result = $string;
    if (is_callable($this->callback))
      $result = call_user_func($this->callback, $string, $params);
    if ($result == $string && isset($this->lang[ $string ]))
      $result = $this->lang[ $string ];
    if (is_array($params) || is_object($params))
      foreach ($params AS $key => $val)
        $result = str_replace('{'.$key.'}', $val, $result);
    return $result;
  }

}