<?php

namespace WebuddhaInc\FormGimp;

class Captcha {

  public static $instance;
  public $app;
  public $db;
  public $lifetime;
  public $session_id;

  // Constructor ----------------------------------------------------------------
  function __construct($app){

    // Required
      if (!$app) {
        throw new Exception('App Undefined');
      }

    // Stage
      $this->app        = $app;
      $this->db         = $app->db;
      $this->lifetime   = 3600;
      $this->session_id = session_id();

  }

  // Get Instance --------------------------------------------------------------
  static function getIntance($app) {
    if (!self::$instance) {
      self::$instance = new self($app);
    }
    return self::$instance;
  }

  // Get Captcha Dialog --------------------------------------------------------
  function getCode(){
    $html = array();
    $html[] = '<div class="formgimp captcha">';
    $html[] = '  <p>Copy letters from the Security Image into the text box. Generate a New Code if you cannot read the letters.</p>';
    $html[] = '  <div class="imgcode">';
    $html[] = '    <img src="" id=security_img />';
    $html[] = '    <input type="text" name="security_value" value="" class="textbox" example="" required="1" autocomplete="off" validate="\w+" label="Security Image" id="security_value" maxlength="12" />';
    $html[] = '    <input type=button class="btn btn-default" onClick=newSecurityImage(); value="Generate Different Code" class="button" />';
    $html[] = '    <script> function newSecurityImage() { document.getElementById(\'security_img\').src = \''. $this->getImageUrl() .'&r=\'+Math.random(); }; newSecurityImage(); </script>';
    $html[] = '  </div>';
    $html[] = '</div>';
    return implode("\n", $html);
  }

  // Get Request Var -----------------------------------------------------------
  function getRequestVar($key, $def=null){
    return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $def;
  }

  // Get URL for Image ---------------------------------------------------------
  function getImageUrl(){
    if( $this->ready() )
      return $this->app->route('task=captcha&format=raw');
    else
      return null;
  }

  // Internal: Check Ready ------------------------------------------------------
  function ready(){
    if( $this->db )
      if( $this->lifetime )
        if( $this->session_id )
          return 1;
        else if($showErrors)
          throw new Exception('Captcha: The session is not set');
      else if($showErrors)
        throw new Exception('Captcha: The lifetime value is not set');
    else if($showErrors)
      throw new Exception('Captcha: The db value is not set');
    return 0;
  }

  // Internal: Create DB Instance ----------------------------------------------
  function create( $security_key ) {
    if( $this->ready() ){
      if( $this->db->query("DELETE FROM `#__wbfg_captcha_session` WHERE (`session_id` = '".$this->session_id."') OR (`created` < '".(time() - $this->lifetime)."');") ){
        if( $this->db->query("INSERT INTO `#__wbfg_captcha_session` VALUES ('".$this->session_id."','".$security_key."','".time()."');") )
          return 1;
      }
    }
    return 0;
  } // create

  // Internal: Validate User Entry ---------------------------------------------
  function checkUserInput( $input_value = null ) {
    if( $this->ready() )
      if( $input_value || $input_value = $this->getRequestVar('security_value') ){
        $this->db->query("SELECT * FROM `#__wbfg_captcha_session` WHERE `session_id` = '".$this->session_id."' AND `created` >= '".(time() - $this->lifetime)."'");
        if ($rows = $this->db->getRows()){
          $row = reset($rows);
          if ($row->key == strtoupper($input_value))
            return 1;
        }
      }
    return 0;
  } // check

} // class secImgCreator