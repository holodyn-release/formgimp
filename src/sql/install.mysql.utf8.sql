
CREATE TABLE IF NOT EXISTS `#__wbfg_captcha_session` (
  `session_id` varchar(200) NOT NULL default '',
  `key` varchar(32) NOT NULL default '',
  `created` varchar(14) NOT NULL default '',
  PRIMARY KEY  (`session_id`)
) COMMENT='secureImgCreator - CrossRef';

CREATE TABLE IF NOT EXISTS `#__wbfg_attachments` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `table` varchar(32) NOT NULL default '',
  `table_id` int(10) unsigned NOT NULL default '0',
  `field` varchar(64) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `type` varchar(32) NOT NULL default '',
  `size` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) COMMENT='FormGimp Attachement Store';

CREATE TABLE IF NOT EXISTS `#__wbfg_cache` (
  `session_id` varchar(64) NOT NULL default '',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `table` varchar(32) NOT NULL default '',
  `data` text NOT NULL,
  PRIMARY KEY  (`session_id`,`table`)
) COMMENT='FormGimp Submission Cache';

CREATE TABLE IF NOT EXISTS `#__wbfg_form_contact` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `fullname` varchar(128) NOT NULL default '',
  `email` varchar(128) NOT NULL default '',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `reviewed` datetime NOT NULL default '0000-00-00 00:00:00',
  `archived` int(1) NOT NULL default '0',
  `phone` varchar(32) default NULL,
  `subject` varchar(64) default NULL,
  `description` text,
  `referrer` varchar(64) default NULL,
  `data` text,
  `history` text,
  PRIMARY KEY  (`id`)
) COMMENT='FormGimp Sample Form Table';

CREATE TABLE IF NOT EXISTS `#__wbfg_index` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `table` varchar(32) NOT NULL default '',
  `file_css` VARCHAR( 255 ) NOT NULL default '',
  `file_form` varchar(255) NOT NULL default '',
  `file_process` varchar(255) NOT NULL default '',
  `name` varchar(64) NOT NULL default '',
  `desc` text NOT NULL,
  `field_types` text NOT NULL,
  `email_alerts` text NOT NULL,
  `secure_img` int(1) unsigned NOT NULL default '0',
  `redirect_url` varchar(255) default NULL,
  `params` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `table` (`table`)
) COMMENT='FormGimp Form Index';

CREATE TABLE IF NOT EXISTS `#__wbfg_setup` (
  `name` varchar(16) NOT NULL default '',
  `title` varchar(32) default NULL,
  `desc` varchar(255) default NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`name`)
) COMMENT='FormGimp Setup';
