<?php

namespace WebuddhaInc\FormGimp;

class DB {

  var $_dbh     = null;
  var $_type    = 'mysqli';
  var $_prefix  = null;
  var $_result  = null;

  public function __construct( $dbh, $prefix = null ) {
    if (!is_a($dbh, 'MySQLi'))
      throw new \Exception('Invalid Database Handler');
    $this->_dbh = $dbh;
    $this->_prefix = (string)$prefix;
  }

  public function prepareQuery( $query ){
    return str_replace('#__', $this->_prefix, $query);
  }

  public function escape( $text ){
    return $this->_dbh->escape_string( $text );
  }

  public function query( $query ){
    $this->_result = $this->_dbh->query($this->prepareQuery($query));
    if( !$this->_result ){
      throw new Exception($this->_dbh->error);
      return false;
    }
    return $this;
  }

  public function count() {
    $this->_result->num_rows;
  }

  public function getValue( $key = null ){
    $row = $this->getRow();
    if ($row) {
      if (is_null($key)) {
        $key = array_keys((array)$row);
        $key = reset($key);
      }
      return isset($row->{$key}) ? $row->{$key} : null;
    }
    return null;
  }

  public function getRow(){
    if ($this->_result){
      return $this->_result->fetch_object();
    }
  }

  public function getRows() {
    $rows = array();
    if ($this->_result) {
      while ($row = $this->getRow()){
        $rows[] = $row;
      }
    }
    return $rows;
  }

  public function getTableFields( $table_id ) {
    $this->query("SHOW COLUMNS FROM `#__wbfg_form_$table_id`");
    $rows = $this->getRows(); $fields = Array();
    foreach( $rows AS $row ) $fields[] = $row->Field;
    return $fields;
  }

  public function getLastInsertID() {
    return $this->_dbh->insert_id;
  }

}
