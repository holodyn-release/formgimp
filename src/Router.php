<?php

namespace WebuddhaInc\FormGimp;

use WebuddhaInc\Params;

class Router {

  public function __construct($app) {
    $this->app = $app;
  }

  public function route($route){
    if ($this->app->config->get('route_callback') && is_callable($this->app->config->get('route_callback'))){
      $res = call_user_func_array($this->app->config->get('route_callback'), array( $route ));
      if ($res !== false)
        return $res;
    }
    $url = $this->app->config->get('route_root', 'app.php');
    if( is_array($route) )
      $url .= http_build_query($route);
    else
      $url .= (strpos($url, '?') !== false ? '&' : '?') . $route;
    return $url;
  }

}