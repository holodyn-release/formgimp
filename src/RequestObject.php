<?php

namespace WebuddhaInc\FormGimp;

use WebuddhaInc\Params;

class RequestObject extends Params {

  public function &get( $key, $def=null, $filter=null ){
    $value = parent::get($key, $def);
    /* Implicit Key Filter */
    if ($value !== $def) {
      switch ($key) {
        case 'table':
        case 'form_id':
          $value = preg_replace('/[^A-Za-z0-9\_]/', '', $value);
          break;
      }
    }
    return $value;
  }

}
