<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

Toolbar::render($task);

$this->app->mediaInclude( 'js', 'js/jquery.js' );
$this->app->mediaInclude( 'js', 'js/default.js' );
$this->app->mediaInclude( 'css', 'css/default.css' );

?>
<div id="formgimp" class="formgimp">
