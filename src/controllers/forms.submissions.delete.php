<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

$id          = $this->app->request->request->get('id');
$table       = $this->app->request->request->get('table');
$cid         = $this->app->request->request->get('cid', array($id));
$_attachPath = $this->app->mediaPath() . '/attachments/';

if( count($cid) && $table ) {
  foreach( $cid AS $iid ) {
    $gimpDB->query("
      SELECT *
      FROM #__wbfg_attachments
      WHERE `table`='".$table."'
        AND `table_id`='".$iid."'
      ");
    $attachments = $gimpDB->getRows(); $rowCount = 1;
    foreach( $attachments AS $attachment ) {
      $attachment = (array)$attachment;
      $filename = md5( $attachment['id'].$attachment['table'].$attachment['table_id'] );
      if( !file_exists($_attachPath.$filename) || unlink($_attachPath.$filename) ) {
        $gimpDB->query("
          DELETE FROM `#__wbfg_attachments`
          WHERE `id`='".$attachment['id']."'
          ");
      }
      else {
        $this->app->redirect('task=forms.submissions&table='.$table, 'Error Deleting Submission Attachment ['.$filename.']');
      }
    }
    $gimpDB->query("
      DELETE FROM `#__wbfg_form_".$table."`
      WHERE `id`='".$iid."'
      ");
  }
}

$this->app->redirect('task=forms.submissions&table='.$table, 'Record(s) Deleted');
