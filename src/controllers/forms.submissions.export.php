<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

require 'forms.submissions.php';

if ($this->app->request->request->get('format') !== 'raw') {
  $this->app->redirect('task=forms.submissions.export&format=raw');
}

$index = Common::getIndexArray($table);

header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename=formgimp-'.$index['table'].'-submissions.csv');
header('Pragma: no-cache');

$formFields = $index['field_types'];
$headers = array(
            'id',
            'created',
            'reviewed',
            'fullname',
            'email',
            '# attachments',
            'phone',
            'referrer',
            'subject',
            'message'
          );
foreach( $formFields AS $field ){
  $field_id = $field['name'];
  if( !in_array($field['type'],array('label','file')) && $field_id && !in_array($field_id,Common::$coreFormFields) )
    $headers[] = $field_id;
}
echo format_csv( $headers )."\r\n";

for($i=0;$i<count($submissions);$i++){
  $row = (array)$submissions[$i];
  $data = array();
  $data[] = strtoupper($row['table']) . $row['id'];
  $data[] = $row['created'];
  $data[] = $row['reviewed'];
  $data[] = $row['fullname'];
  $data[] = $row['email'];
  $data[] = $row['total'];
  $data[] = $row['phone'];
  $data[] = $row['referrer'];
  $data[] = $row['subject'];
  $data[] = $row['description'];
  $values = unserialize($row['data']);
  foreach( $formFields AS $field ){
    $field_id = $field['name'];
    if( !in_array($field['type'],array('label','file')) && $field_id && !in_array($field_id,Common::$coreFormFields) )
      $data[] = (is_array($values[ $field_id ])?join('; ',$values[ $field_id ]):$values[ $field_id ]);
  }
  echo format_csv( $data )."\r\n";
}

exit();

// ****************************************************
function format_csv($fields=array(), $delimiter=',', $enclosure = '"'){
  $str = '';
  $escape_char = '\\';
  foreach ($fields as $value){
    if (strpos($value, $delimiter) !== false ||
        strpos($value, $enclosure) !== false ||
        strpos($value, "\n") !== false ||
        strpos($value, "\r") !== false ||
        strpos($value, "\t") !== false ||
        strpos($value, ' ') !== false)
    {
      $str2 = $enclosure;
      $escaped = 0;
      $len = strlen($value);
      for ($i=0;$i<$len;$i++)
      {
        if ($value[$i] == $escape_char)
          $escaped = 1;
        else if (!$escaped && $value[$i] == $enclosure)
          $str2 .= $enclosure;
        else
          $escaped = 0;
        $str2 .= $value[$i];
      }
      $str2 .= $enclosure;
      $str .= $str2.$delimiter;
    }
    else
      $str .= $value.$delimiter;
  }
  return substr($str,0,-1);
} // format_csv
