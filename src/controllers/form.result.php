<?php namespace WebuddhaInc\FormGimp;

use \WebuddhaInc\Params;

$index  = Common::getIndexArray( $this->app->request->request->get('form_id') );
$params = new Params($index['params']);

?>
<div class="form_result">
  <?php if($params->get('show_title')){ ?><h1><?= $index['name'] ?></h1><?php } ?>
  <?php echo (empty($index['confirmation_msg']) ? $GLOBALS['fg_confirmation_msg'] : $index['confirmation_msg']) ?>
</div>
