<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

// Table Fields
  $indexFields = Array(
    'table',
    'file_css',
    'file_form',
    'file_process',
    'name',
    'desc',
    'field_types',
    'email_alerts',
    'confirmation_msg',
    'secure_img',
    'redirect_url',
    'params'
  );
  $requiredFields = Array(
    'table' => 'Unique Form ID',
    'name'  => 'Form Name'
  );

// Collect Post
  $POST = array(
    'id' => $this->app->request->request->get('id')
    );
  foreach($indexFields AS $field) {
    $POST[$field] = $this->app->request->request->get($field);
  }

// Email Alerts
  $email_alerts = array();
  $set = explode("\n",preg_replace('/\r/','',$this->app->request->request->get('email_alerts')));
  $POST['email_alerts'] = '';
  foreach( $set AS $email ){
    $email = trim($email);
    if( strlen($email) )
      $email_alerts[] = $email;
  }
  $POST['email_alerts'] = serialize($email_alerts);

// Form Fields
  $fieldOrder = 1;
  $fieldTypes = array();
  foreach ($this->app->request->request AS $key => $val) {
    if( preg_match('/^field(\d+)/',$key,$match) && is_array($val) ) {
      $name = preg_replace(
        '/\_+/',
        '_',
        preg_replace('/[^\w\d]+/','_',$val['name'])
      );
      if( strlen($name) ){
        switch( $val['type'] ){
          case 'list':
          case 'radio':
          case 'checkbox':
            $val['options'] = trim($val['options']);
            if(!strlen($val['options']))
              $val['options'] = "0|No\n1|Yes";
          break;
        }
        $val = (array)Form::getFieldParams($val);
        $fieldTypes[] = array(
          'ordering'=>$fieldOrder++,
          'type'     => $val['type'],
          'name'     => $name,
          'label'    => $val['label'],
          'tooltip'  => $val['tooltip'],
          'visible'  => $val['visible'] ? 1 : 0,
          'required' => $val['required'] ? 1 : 0,
          'validate' => $val['validate'],
          'options'  => $val['options']
        );
      }
    }
  }
  $POST['field_types'] = serialize($fieldTypes);

// Parameters
  $params = $this->app->request->request->get('params');
  if( is_array( $params ) )
    $POST['params'] = serialize( $params );
  else
    $POST['params'] = serialize( array() );
  $POST['table'] = preg_replace('/\_+/','_',preg_replace('/[^\w\d]+/','_',$this->app->request->request->get('table')));

// Clean Post
  foreach( $POST AS $k => $v ) {
    if( !is_array($v) ) {
      $POST[$k] = $gimpDB->escape(trim($v));
    }
  }

// Catch Errors
  $errorMsgs = array();
  foreach( $requiredFields AS $k => $v )
    if( !strlen($POST[ $k ]) )
      $errorMsgs[] = 'Missing Value - '.$v;

// Post or Error
  if(count($errorMsgs)){
    echo $this->app->renderMsg('error', join('<br>',$errorMsgs));
    include('forms.edit.php');
  }
  else {
    if( (int)$POST['id'] ){
      $fields = array();
      foreach( $indexFields AS $field )
        if( isset( $POST[ $field ] ) )
          $fields[] = "`".$field."` = '".$POST[ $field ]."'";
      $sql = "UPDATE #__wbfg_index SET ".join(', ',$fields). " WHERE `id`='".$POST['id']."'";
    }
    else {
      $fields = $values = array();
      foreach( $indexFields AS $field )
        if( isset( $POST[ $field ] ) ){
          $fields[] = $field;
          $values[] = $POST[ $field ];
        }
      $sql = "INSERT INTO #__wbfg_index (`".join('`,`',$fields)."`) VALUES ('".join("', '",$values)."')";
    }
    $gimpDB->query($sql);
    $gimpDB->query("
      CREATE TABLE IF NOT EXISTS `#__wbfg_form_".$POST['table']."` (
        `id` int(10) unsigned NOT NULL auto_increment,
        `fullname` varchar(128) NOT NULL default '',
        `email` varchar(128) NOT NULL default '',
        `created` datetime NOT NULL default '0000-00-00 00:00:00',
        `reviewed` datetime NOT NULL default '0000-00-00 00:00:00',
        `archived` int(1) NOT NULL default '0',
        `phone` varchar(32) default NULL,
        `subject` varchar(64) default NULL,
        `description` text,
        `referrer` varchar(64) default NULL,
        `data` text,
        `history` text,
        PRIMARY KEY  (`id`)
      );
    ");
    if (preg_match('/apply/', $task))
      $this->app->redirect('task=forms.edit&table=' . $POST['table'], 'Form Saved');
    $this->app->redirect('task=forms', 'Form Saved');
  }
