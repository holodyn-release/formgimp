<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

$setup_keys = array(
  'auto_archive',
  'recaptcha_key',
  'recaptcha_secret',
  'archive_timeout',
  'email_from',
  'confirmation_msg'
  );
foreach ($setup_keys AS $setup_key) {
  Common::saveSetupKey( $setup_key, $this->app->request->request->get($setup_key) );
}

$this->app->redirect('task=setup', 'Update Successful');
