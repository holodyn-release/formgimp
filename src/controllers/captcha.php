<?php

namespace WebuddhaInc\FormGimp;

// *************************************************************************************************** Stage
$secImg    = new Captcha($this->app);
$SI_CONFIG = new \stdClass();

// *************************************************************************************************** CONFIGURATION

// Absolute Path
$SI_CONFIG->absolutePath    =  $this->app->mediaPath() . '/captcha';

// Size of final Ouput
$SI_CONFIG->imgWidth        =  $secImg->getRequestVar( 'imgWidth', 150 );
$SI_CONFIG->imgHeight       =  $secImg->getRequestVar( 'imgHeight', 50);

// Number of Background Images available
$SI_CONFIG->numBackgrounds  =  $secImg->getRequestVar( 'numBackgrounds', 0 );

// Character Setup
$SI_CONFIG->numChars        =  $secImg->getRequestVar( 'numChars', 6 );
$SI_CONFIG->charArray       =  getCharArray();

// Random Lines Setup
$SI_CONFIG->numLines        =  $secImg->getRequestVar( 'numLines', 0 );

// Available Fonts
$SI_CONFIG->runImageSkew    = false;
$SI_CONFIG->runColorAvg     = true;
$SI_CONFIG->grayScaleFont   = true;
$SI_CONFIG->fonts           = array(
                                array(
                                  'path' => 'TTF_Fonts/BAGABT__.TTF',
                                  'size' => 20
                                  )
                                );

// *************************************************************************************************** APPLICATION

// Create BG Image -------------------------------------------------------------------------
$bgImg = @ImageCreateFromJPEG($SI_CONFIG->absolutePath.'/img/bg_'.rand(0,$SI_CONFIG->numBackgrounds).".jpg");
$im    = @ImageCreateTrueColor( $SI_CONFIG->imgWidth, $SI_CONFIG->imgHeight );
$white = ImageColorAllocate($im, 255, 255, 255); imagefill($im, 0, 0, $white);
ImageCopyMerge( $im, $bgImg, 0, 0, rand(0,(imagesx($bgImg)-$SI_CONFIG->imgWidth)), rand(0,(imagesy($bgImg)-$SI_CONFIG->imgHeight)), $SI_CONFIG->imgWidth, $SI_CONFIG->imgHeight, 100 );

// Prepare for Characters ------------------------------------------------------------------
$SI_CONFIG->charW  = round( ($SI_CONFIG->imgWidth * .9) / $SI_CONFIG->numChars );
$SI_CONFIG->charH  = round( $SI_CONFIG->imgHeight * .8 );;
$SI_CONFIG->px     = (imagesx($im) - $SI_CONFIG->charW * $SI_CONFIG->numChars) / 2;
$SI_CONFIG->py     = (imagesy($im) - $SI_CONFIG->charH) / 2;

if( $SI_CONFIG->numLines )
  imageGenerateRandomLines( $im, $SI_CONFIG );

if( $SI_CONFIG->numChars )
  $security_key = imageGenerateRandomChars( $im, $SI_CONFIG );

// Create SI Database Record ---------------------------------------------------------------
if( $secImg->create( $security_key ) ){

  // Render Image --------------------------------------------------------------------------
  header("Content-type: image/jpeg");
  imagepng($im);
  imagedestroy($im);

} else {

  // Return Error --------------------------------------------------------------------------
  header("Content-type: image/jpeg");
  imagedestroy($im);
  $im = @imagecreatefromjpeg($SI_CONFIG->absolutePath.'/img/error.jpg');
  imagepng($im);
  imagedestroy($im);

}

exit();

// *************************************************************************************************** FUNCTIONS

// Get Array of Characters -----------------------------------------------------------------
function getCharArray( $aN=1, $aL=0, $aH=1 ){
  $charArray = Array();
  if( $aN )
    for( $i=50; $i <= 57; $i++ )  $charArray[] = chr( $i );  // Numeric 2-9
    // for( $i=48; $i <= 57; $i++ )  $charArray[] = chr( $i );  // Numeric
  if( $aL )
    for( $i=97; $i <= 122; $i++ ) $charArray[] = chr( $i );  // Alpha Lower
  if( $aH )
    for( $i=65; $i <= 90; $i++ )  $charArray[] = chr( $i );  // Alpha Upper
  return $charArray;
}

// Random Lines ----------------------------------------------------------------------------
function imageGenerateRandomLines( &$im, &$CONFIG ){
  $lineImg = @imagecreate( imagesx($im), imagesy($im) );
  $tranPtr = imagecolorallocate($lineImg, 255, 255, 255);
  for( $i=0;$i < $CONFIG->numLines; $i++ ){
    $lineCol = imagecolorallocate( $lineImg, rand(0,255), rand(0,255), rand(0,255) );
    imageline( $lineImg,
      rand(0,imagesx($im)),
      rand(0,imagesy($im)),
      rand(0,imagesx($im)),
      rand(0,imagesy($im)),
      $lineCol
      );
  }
  imagecolortransparent( $lineImg, $tranPtr );
  imagecopymerge( $im, $lineImg, 0, 0, 0, 0, imagesx($im), imagesy($im), 65 );
  imagedestroy( $lineImg );
}

// Random Characters -----------------------------------------------------------------------
function imageGenerateRandomChars( &$im, &$CONFIG ){
  $security_key = '';
  for( $i=0;$i < $CONFIG->numChars; $i++ ){
    $charX = $CONFIG->px+($i*$CONFIG->charW);
    $charY = $CONFIG->py+rand(-5,20);
    $char = $CONFIG->charArray[ rand(0,count($CONFIG->charArray)-1) ];
    $security_key .= $char;
    $charImg = @ImageCreate( $CONFIG->charW, $CONFIG->charH );
    $tranPtr = ImageColorAllocate($charImg,128,128,128);
    ImageColorTransparent($charImg, $tranPtr);
    $thisFont = $CONFIG->fonts[ rand(0,count($CONFIG->fonts)-1) ];
    if( $CONFIG->runColorAvg ){
      // Locate Color of Underlying Image to Determine Font Color
      $cGrey = getColorAverage( $im, $charX, $charY, $CONFIG->charW, $CONFIG->charH );
      if( $CONFIG->grayScaleFont ){
        $sColor = ($cGrey>123?rand(0,60):rand(195,255));
        $charCol = ImageColorAllocate( $charImg, $sColor, $sColor, $sColor );
      } else {
        $charCol = ImageColorAllocate( $charImg,
          ($cGrey>123?rand(0,60):rand(195,255)),
          ($cGrey>123?rand(0,60):rand(195,255)),
          ($cGrey>123?rand(0,60):rand(195,255)) );
      }
    } else {
      // Default Color to Black
      $charCol = ImageColorAllocate($charImg, 0, 0, 0);
    }
    ImageTTFText(
      $charImg,
      $thisFont['size'], 0, 0,
      $CONFIG->charH/2, $charCol,
      $CONFIG->absolutePath.'/'.$thisFont['path'],
      $char);
    if( $CONFIG->runImageSkew )
      imageSkew( $charImg, rand( -10, 10 ), 255 );
    ImageCopyMerge( $im, $charImg, $charX, $charY, 0, 0, $CONFIG->charW, $CONFIG->charH, rand( 50, 100 ) );
    ImageDestroy( $charImg );
  }
  return $security_key;
}

// Skew Image ------------------------------------------------------------------------------
function imageSkew( &$im, $angle, $matte=255 ) {
  if( $angle > 45 ) $angle = 45;
    else if( $angle < -45 ) $angle = -45;
  $srcW     = imagesx( $im );
  $srcH     = imagesy( $im );
  $angleRad = abs($angle) * (pi()/180);
  $padWidth = abs(tan($angleRad) * $srcH);
  $imgW     = floor($srcW+$padWidth);
  $imgH     = $srcH;
  $newImg   = @imagecreate( $imgW, $imgH );
  $tranPtr  = ImageColorAllocate($newImg, $matte, $matte, $matte);
  for($row=0; $row<$imgH;$row++) {
    $left = (int) abs(tan($angleRad) * ($row+1)) - 1;
    if( $angle > 0 ) $left = $imgW - ($srcW + $left);
    imagecopymerge( $newImg, $im, $left, $row, 0, $row, 20, 1, 100 );
  }
  imagecolortransparent( $newImg, $tranPtr );
  imagedestroy($im);
  $im = $newImg;
}

// Color Average ---------------------------------------------------------------------------
function getColorAverage( &$im, $x, $y, $w, $h ){
  $cGrey = 0; $count = 0;
  if ($imRGB = @imagecolorat($im, $x, $y )){
    $count++;
    $cGrey += floor(( (($imRGB >> 16) & 0xFF) + (($imRGB >> 8) & 0xFF) + ($imRGB & 0xFF) )/3);
  }
  if ($imRGB = @imagecolorat($im, $x + $w, $y )){
    $count++;
    $cGrey += floor(( (($imRGB >> 16) & 0xFF) + (($imRGB >> 8) & 0xFF) + ($imRGB & 0xFF) )/3);
  }
  if ($imRGB = @imagecolorat($im, $x, $y + $h )){
    $count++;
    $cGrey += floor(( (($imRGB >> 16) & 0xFF) + (($imRGB >> 8) & 0xFF) + ($imRGB & 0xFF) )/3);
  }
  if ($imRGB = @imagecolorat($im, $x + $w, $y + $h )){
    $count++;
    $cGrey += floor(( (($imRGB >> 16) & 0xFF) + (($imRGB >> 8) & 0xFF) + ($imRGB & 0xFF) )/3);
  }
  if ($imRGB = @imagecolorat($im, $x + ($w/2), $y + ($h/2) )){
    $count++;
    $cGrey += floor(( (($imRGB >> 16) & 0xFF) + (($imRGB >> 8) & 0xFF) + ($imRGB & 0xFF) )/3);
  }
  return (int)($cGrey / $count);
}