<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

$id = (int)Common::aes_decrypt(str_replace(' ', '+', $this->app->request->request->get('eid')));
if ($id) {
  $attachment = $gimpDB->query("
    SELECT *
    FROM #__wbfg_attachments
    WHERE `id`='". $id ."'
    ")->getRow();
  if ($attachment) {
    $filename = md5( $attachment->id . $attachment->table . $attachment->table_id );
    $filepath = $this->app->attachPath() . '/' . $filename;
    if (is_readable($filepath) && $fh = fopen($filepath, "rb")) {
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header('Content-Disposition: inline; filename="'.addslashes($attachment->filename).'";');
      header("Content-Type: ".$attachment->type);
      header("Content-Length: ".$attachment->size);
      header("Content-Transfer-Encoding: binary");
      while (!feof($fh))
        echo fread($fh, 1024);
      fclose($fh);
    }
  }
}
exit();
