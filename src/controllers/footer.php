<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

?>
  <div class="copyright">
    &copy;<?php echo date('Y') ?> Webuddha.com, Holodyn Atlanta
  </div>
</div>
