<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

$table = $this->app->request->request->get('table');
if ($table) {
  $index = Common::getIndexArray($table);
  if ($index['table']) {
    do {
      $gimpDB->query("
        SELECT *
        FROM #__wbfg_attachments
        WHERE `table`='".$index['table']."'
        LIMIT 100
        ");
      $attachments = $gimpDB->getRows(); $rowCount = 1;
      foreach( $attachments AS $attachment ) {
        $attachment = (array)$attachment;
        $filename = md5( $attachment['id'].$attachment['table'].$attachment['table_id'] );
        if( file_exists($_attachPath.$filename) ) {
          if (!@unlink($_attachPath.$filename)) {
            $this->app->redirect('task=forms', 'Error Deleting Sumission Attachment');
          }
        }
        $gimpDB->query("
          DELETE FROM `#__wbfg_attachments`
          WHERE `id`='".$attachment['id']."'
          ");
      }
    } while($attachments);
    $gimpDB->query("DROP TABLE `#__wbfg_form_".$index['table']."`");
    $gimpDB->query("DELETE FROM `#__wbfg_index` WHERE `table`='".$index['table']."'");
    $this->app->redirect('task=forms', 'Form Deleted Successfully');
  }
  else {
    $this->app->redirect('task=forms', 'Invalid Form Idenfier');
  }
}
else {
  $this->app->redirect('task=forms', 'Invalid Form Idenfier');
}
