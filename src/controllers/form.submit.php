<?php namespace WebuddhaInc\FormGimp;

use \WebuddhaInc\Params;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

$app     =& $this->app;
$config  =& $this->app->config;
$request =& $this->app->request;
$form_id = $request->request->get('form_id');
$return  = urldecode($request->request->get('return', $this->app->route('task=form&form_id='.$form_id)));

// -------------------------------------------------------------------------
// Error if no form specified
// -------------------------------------------------------------------------

if( !$form_id ){
  if ($return) {
    $this->app->redirect($return, $this->app->lang->get('No Form ID Specified'));
  }
  else {
    throw new Exception($this->app->lang->get('No Form ID Specified'));
  }
  return;
}

// ---------------------------------------------------------------------------
//                                                                   Load Form
// ---------------------------------------------------------------------------

$index = Common::getIndexArray( $form_id );

// ---------------------------------------------------------------------------
//                                                                  Use Custom
// ---------------------------------------------------------------------------

if( $index['id'] ){

  $fields      = $gimpDB->getTableFields($form_id);
  $values      = (array)$request->request->get('field', array());
  $attachments = Array();
  $_attachPath = $app->attachPath() . '/';
  $params      = new Params($index['params']);
  $formFields  = $index['field_types'];
  $customData  = array();

  // -------------------------------------------------------------------------
  //                                                 TODO: Save Session Cache
  // -------------------------------------------------------------------------

  Common::saveTableCache( session_id(), $form_id );

  // -------------------------------------------------------------------------
  //                                                                 ReCaptcha
  // -------------------------------------------------------------------------

  if( $index['secure_img'] ) {
    if (class_exists('\ReCaptcha\ReCaptcha') && $GLOBALS['fg_recaptcha_key'] && $GLOBALS['fg_recaptcha_secret']) {
      $reCaptcha = new \ReCaptcha\ReCaptcha($fg_recaptcha_secret);
      $reCaptchaResult = $reCaptcha->verify($this->app->request->request->get('g-recaptcha-response'), $_SERVER['REMOTE_ADDR']);
      if (!$reCaptchaResult->isSuccess()) {
        $this->app->redirect($return, $this->app->lang->get('Invalid Captcha Verification: {error}', array('error' => implode(', ',$reCaptchaResult->getErrorCodes()))));
      }
    }
    else {
      $captcha = new Captcha($this->app);
      if (!$captcha->checkUserInput()) {
        $this->app->redirect($return, $this->app->lang->get('Invalid Captcha Verification Code'));
      }
    }
  }

  // -------------------------------------------------------------------------
  //                                                           Validate Fields
  // -------------------------------------------------------------------------

  foreach (Common::$coreFormFields AS $key) {
    $values[$key] = isset($values[$key]) ? $values[$key] : null;
  }
  foreach( $formFields AS $field ){
    if( $field['name'] && !in_array($field['type'],array('file','label')) ){
      if( array_key_exists($field['name'],$values) ) {
        $customData[ $field['name'] ] = is_array($values[$field['name']]) ? $values[$field['name']] : trim($values[$field['name']]);
      }
      $isEmpty = !isset($customData[$field['name']])
                || (is_array($customData[$field['name']]) && !$customData[$field['name']])
                || (is_string($customData[$field['name']]) && !strlen($customData[$field['name']]))
                ;
      if( $isEmpty && $field['visible'] && $field['required'] ){
        if ($return) {
          $this->app->redirect($return, $this->app->lang->get('Please Provide a Valid {label}', $field));
        }
        else {
          throw new Exception($this->app->lang->get('Please Provide a Valid {label}', $field));
        }
      }
    }
  }

  // -------------------------------------------------------------------------
  //                                                         Insert Submission
  // -------------------------------------------------------------------------

  $gimpDB->query("
    INSERT INTO `#__wbfg_form_$form_id` (
    `created`,
    `fullname`,
    `email`,
    `phone`,
    `referrer`,
    `subject`,
    `description`,
    `data`
    ) VALUES (
    '".date('Y-m-d H:i:s')."',
    '".$gimpDB->escape($values['fullname'])."',
    '".$gimpDB->escape($values['email'])."',
    '".$gimpDB->escape($values['phone'])."',
    '".$gimpDB->escape($values['referrer'])."',
    '".$gimpDB->escape($values['subject'])."',
    '".$gimpDB->escape($values['description'])."',
    '".$gimpDB->escape(serialize($values))."'
    );
  ");

  // Get Last Insert ID
  $table_id = $gimpDB->getLastInsertID();

  // -------------------------------------------------------------------------
  //                                                       Collect Attachments
  // -------------------------------------------------------------------------
  if( !empty($table_id) ) {

    foreach( $_FILES AS $field_name => $FILES ){
      if( count( $FILES['name'] ) > 1 ){
        for( $i=0; $i < count($FILES['name']); $i++ ){
          if( $FILES['name'][$i] && is_uploaded_file($FILES['tmp_name'][$i]) ){
            $gimpDB->query("INSERT INTO `#__wbfg_attachments` (`table`,`table_id`,`field`,`filename`,`type`,`size`) VALUES ('$form_id','$table_id','$field_name','".$FILES['name'][$i]."','".$FILES['type'][$i]."','".$FILES['size'][$i]."')");
            $attach_id = $gimpDB->getLastInsertID();
            $filename = md5( $attach_id.$form_id.$table_id );
            if( !move_uploaded_file($FILES['tmp_name'][$i],$_attachPath . $filename) ) {
              $gimpDB->query("DELETE FROM `#__wbfg_attachments` WHERE `table`='$form_id' AND `table_id`='$table_id' AND `id`='$attach_id';");
            }
          }
        }
      }
      else if( $FILES['name'] && is_uploaded_file($FILES['tmp_name']) ){
        $gimpDB->query("INSERT INTO `#__wbfg_attachments` (`table`,`table_id`,`field`,`filename`,`type`,`size`) VALUES ('$form_id','$table_id','$field_name','".$FILES['name']."','".$FILES['type']."','".$FILES['size']."')");
        $attach_id = $gimpDB->getLastInsertID();
        $filename = md5( $attach_id.$form_id.$table_id );
        if( !move_uploaded_file($FILES['tmp_name'],$_attachPath . $filename) ) {
          $gimpDB->query("DELETE FROM `#__wbfg_attachments` WHERE `table`='$form_id' AND `table_id`='$table_id' AND `id`='$attach_id';");
        }
      }
    }

  }

  // -------------------------------------------------------------------------
  //                                                            Submission
  // -------------------------------------------------------------------------

  $submission = Common::loadSubmission( $form_id, $table_id );

  // -------------------------------------------------------------------------
  //                                                            Output Content
  // -------------------------------------------------------------------------

  $content = new \stdClass();
  $content->title = $params->get('title');
  $content->text  = (empty($index['confirmation_msg']) ? $GLOBALS['fg_confirmation_msg'] : $index['confirmation_msg']);

  // -------------------------------------------------------------------------
  //                                               TODO: Custom Form Processor
  // -------------------------------------------------------------------------

  if ($processor = $this->app->mediaLookup('processor', (@$index['file_process'] ?: $form_id) . '.php')) {
    try {
      $processor_config = $params->get('processor_config') ? @json_decode($params->get('processor_config')) : null;
      require($processor->path);
    } catch (Exception $e) {
      $this->app->redirect($return, $this->app->lang->get('Form Processor Error: {error}', array('error' => $e->getMessage())));
    }
  }

  // -------------------------------------------------------------------------
  //                                                        Email Confirmation
  // -------------------------------------------------------------------------

  if ($params->get('email_confirm')){
    if ($confirm_email = $this->app->mediaLookup('email', $params->get('email_confirm_id', 'confirm').'.php')) {
      ob_start();
      require($confirm_email->path);
      $html_output = ob_get_clean();
      Common::send_mail(
        $values['email'],
        $GLOBALS['fg_email_from'],
        $this->app->lang->get('Confirmation of your Submission'),
        $html_output
      );
    }
    else {
      throw new Exception($this->app->lang->get('Confirmation Email Not Found'));
    }
  }

  // -------------------------------------------------------------------------
  //                                                               Email Alert
  // -------------------------------------------------------------------------

  if ($params->get('email_alert')){
    $email_alerts = $index['email_alerts'];
    if ($email_alerts && $alert_email = $this->app->mediaLookup('email', $params->get('email_alert_id', 'alert').'.php')) {
      ob_start();
      require($alert_email->path);
      $html_output = ob_get_clean();
      for($i=0;$i<count($email_alerts);$i++){
        $subject = '['.strtoupper($form_id).$table_id.'] '.$values['fullname'];
        Common::send_mail(
          $email_alerts[$i],
          $GLOBALS['fg_email_from'],
          $subject,
          $html_output
        );
      }
    }
  }

  // -------------------------------------------------------------------------
  //                                                TODO: Delete Session Cache
  // -------------------------------------------------------------------------

  Common::deleteTableCache( session_id(), $form_id );

  // -------------------------------------------------------------------------
  //                                                              Final Output
  // -------------------------------------------------------------------------

  if ($params->get('submit_form_id')){
    $this->app->redirect('task=form&form_id='.$params->get('submit_form_id'));
  }
  else if (@$index['redirect_url']) {
    $this->app->redirect($index['redirect_url']);
  }
  else if ($return) {
    $return = add_query_arg('task', 'form.result', $return);
    $this->app->redirect($return);
  }
  else {
    $this->app->redirect('task=form.result&form_id='.$form_id);
  }

}
else {

  throw new Exception($this->app->lang->get('Error Submitting Form {form_id} does not exist', array('form_id' => $form_id)));

}