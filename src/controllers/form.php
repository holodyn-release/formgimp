<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

$form_id = $this->app->request->request->get('form_id', $this->app->request->request->get('table'));
$index = Common::getIndexArray($form_id);
if (!$index) {
  echo $this->app->renderMsg(400, "Form Not Found [{$form_id}]");
  return;
}

echo Form::render( $index, array(
  'render_custom' => true
  ) );
