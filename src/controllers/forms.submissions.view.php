<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

$index = Common::getIndexArray($this->app->request->request->get('table'));
if (!$index) {
  $this->app->redirect('task=forms.sumissions', 'Invalid Request');
}

$gimpDB->query("
  UPDATE `#__wbfg_form_". $index['table'] ."`
  SET `reviewed`='".date('Y-m-d H:i:s')."'
  WHERE `id`='".$_REQUEST['id']."'
  ");
$gimpDB->query("
  SELECT *
  FROM `#__wbfg_form_".$index['table']."`
  WHERE `id`='". (int)$this->app->request->request->get('id') ."'
  ");
$submission = $gimpDB->getRow();
if (!$submission) {
  $this->app->redirect('task=forms.sumissions', 'Invalid Record');
}
$submission = (array)$submission;

?>
<?php include 'header.php'; ?>
<form action="<?= Common::gimpLink('task=forms.edit') ?>" method=POST name="adminForm" id="adminForm">
<input type=hidden name=task value="forms.submissions" />
<input type=hidden name=table value="<?= $index['table'] ?>" />
<input type=hidden name=id value="<?= $submission['id'] ?>" />
</form>
<table border=0 cellpadding=5 cellspacing=0 width=100% class=data>
  <tr>
    <th width=300>Details</th>
    <th>Attachments</th>
  </tr>
  <tr>
    <td valign=top width=50%>
      <table border=0 cellpadding=2 cellspacing=0 width=100% class=attachments>
        <tr>
          <th NOWRAP>Full Name:</td>
          <td><?= Form::htmlOut($submission['fullname']) ?></td>
        </tr>
        <tr>
          <th NOWRAP>Email:</td>
          <td><a href="mailto:<?= Form::paramOut($submission['fullname']) ?> (<?= Form::paramOut($submission['email']) ?>)" title="Send Email"><?= Form::htmlOut($submission['email']) ?></a></td>
        </tr>
        <tr>
          <th NOWRAP>Phone:</td>
          <td><?= Form::htmlOut($submission['phone']) ?></td>
        </tr>
        <tr>
          <th NOWRAP>Referrer:</td>
          <td><?= Form::htmlOut($submission['referrer']) ?></td>
        </tr>
        <tr>
          <th NOWRAP>Subject:</td>
          <td><?= Form::htmlOut($submission['subject']) ?></td>
        </tr>
        <tr>
          <th NOWRAP>Description:</td>
          <td><div style="white-space: pre;"><?= Form::htmlOut($submission['description']) ?></div></td>
        </tr>
        <?php
          $formFields = (array)@$index['field_types'];
          if( !is_array($formFields) )
            $formFields = array();
          $values = unserialize($submission['data']);
          if( !is_array($values) ) $value = array();
          foreach( $formFields AS $field ){
            if( !in_array($field['type'],array('label','file')) && @$field['name'] && !in_array($field['name'], ) ){
              ?>
              <tr>
                <th valign=top><?= Form::htmlOut($field['label']) ?>:</th>
                <td><?= Form::htmlOut(is_array($values[ $field['name'] ])?join(', ',$values[ $field['name'] ]):$values[ $field['name'] ]) ?></td>
              </tr>
              <?php
            }
          }
        ?>
        <tr><td colspan=2><hr/></td></tr>
        <tr>
          <th NOWRAP>Created:</td>
          <td><?= $submission['created'] ?></td>
        </tr>
        <tr>
          <th NOWRAP>Reviewed:</td>
          <td><?= $submission['reviewed'] ?></td>
        </tr>
        <tr>
          <th NOWRAP>Archived:</td>
          <td><?= $submission['archived'] ? 'YES' : 'NO' ?></td>
        </tr>
        <tr>
          <th NOWRAP>Private Link:</td>
          <td><a href="<?php
            echo $public_url = $this->app->route('task=submission&xid='.Common::aes_encrypt($index['table'].','.$submission['id']));
            ?>"><?= $public_url ?></td>
        </tr>
      </table>
    </td>
    <td valign=top>
      <table border=0 cellpadding=0 cellspacing=0 width=100% class=attachments>
        <?php
          $gimpDB->query("
            SELECT *
            FROM #__wbfg_attachments
            WHERE `table`='".$_REQUEST['table']."'
              AND `table_id`='".$_REQUEST['id']."'
            ORDER BY `field`, `filename`, `size` DESC
            ");
          $attachments = $gimpDB->getRows(); $rowCount = 1;
          foreach( $attachments AS $attachment ){
            $attachment = (array)$attachment;
            $fileName = $attachment['filename'];
            $download_url = $this->app->route('task=download&format=raw&eid='.Common::aes_encrypt($attachment['id']));
            $thumbnail_url = $this->app->route('task=thumbnail&format=raw&eid='.Common::aes_encrypt($attachment['id']));
            if( strlen($fileName) > 48 ) $fileName = preg_replace('/^(.{48}).*$/','$1',$fileName).'...';
            echo '<tr>';
            echo '<td>'.$rowCount++.') </td>';
            echo '<td>'.$attachment['field'].'</td>';
            echo '<td><a href="'. $download_url .'" title="'.$attachment['filename'].'">'.$fileName.'</a></td>';
            echo '<td>'.Common::formatFileSize($attachment['size']).'</td>';
            if( preg_match('/(jpg|jpeg|gif|png|bmp)$/', strtolower($attachment['filename']), $match) ){
              ?>
              <td>
                <img src="<?= $thumbnail_url ?>" style="max-width: 320px; max-height: 240px;" />
              </td>
              <?php
            }
            else if( preg_match('/(mp4|flv|mpeg|vob|ogg|ogv|avi|mov|qt|wmv|m4v)$/', strtolower($attachment['filename']), $match) ){
              ?>
              <td>
                <video id="sampleMovie" width="320" controls>
                  <source src="<?= $thumbnail_url ?>" />
                </video>
              </td>
              <?php
            }
            else
              echo '<td>&nbsp;</td>';
            echo '</tr>';
          }
        ?>
      </table>
    </td>
  </tr>
</table>
<?php include 'footer.php'; ?>
