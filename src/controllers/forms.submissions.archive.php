<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

if( !isset($archive) ) $archive = 1;

$id          = $this->app->request->request->get('id');
$table       = $this->app->request->request->get('table');
$cid         = $this->app->request->request->get('cid', array($id));

$message = ($archive ? 'Archive' : 'Unarchive');
if( count($cid) && $table ){
  $res = $gimpDB->query("
    UPDATE `#__wbfg_form_".$table."`
    SET `archived`='".$archive."'
    WHERE `id` IN ('".join("','",$cid)."')
    ");
  $message .= ' Successful';
}
else {
  $message .= ' Failed';
}

if( $id ){
  $this->app->redirect('task=forms.submissions.view&table='.$table.'&id='.$id, $message);
}
else {
  $this->app->redirect('task=forms.submissions&table='.$table, $message);
}
