<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

$list_start = $this->app->request->getSessionRequest("list_start_{$task}", 'list_start', 0);
$list_limit = $this->app->request->getSessionRequest("list_limit_{$task}", 'list_limit', 20);

$gimpDB->query("SELECT COUNT(*) AS total FROM #__wbfg_index;");
$rows = $gimpDB->getRows();
$total = $rows[0]->total;

$list_limit = $list_limit >= 0 ? $list_limit : 20;
$list_start = $list_start >= 0 && ($list_start * $list_limit < $total) ? $list_start : 0;

$gimpDB->query("SELECT * FROM #__wbfg_index ORDER BY name LIMIT $list_start, $list_limit");
$forms = $gimpDB->getRows();

?>
<?php include 'header.php'; ?>
<?php Form::showPageNav( $total, $list_start, $list_limit ); ?>
<table border=0 cellpadding=5 cellspacing=0 width=100% class=data>
  <colgroup>
    <col class="form_table">
    <col class="form_name">
    <col class="last_activity">
    <col class="new_count">
    <col class="active_count">
    <col class="archived_count">
  </colgroup>
  <thead>
    <tr>
      <th width=1% NOWRAP>Form ID</th>
      <th>Form Name</th>
      <th width=1% NOWRAP>Last Activity</th>
      <th width=1% NOWRAP># New</th>
      <th width=1% NOWRAP># Active</th>
      <th width=1% NOWRAP># Archived</th>
    </tr>
  </thead>
  <tbody>
    <?php
      foreach( $forms AS $form ){
        $form = (array)$form;

        $table = '#__wbfg_form_'.$form['table'];

        $gimpDB->query("SELECT COUNT(*) AS `total` FROM `$table` WHERE `reviewed`='0000-00-00 00:00:00'");
          $new = $gimpDB->getValue();

        $gimpDB->query("SELECT `created` FROM `$table` ORDER BY `created` LIMIT 1");
          $last = $gimpDB->getValue() ?: '- none -';

        $gimpDB->query("SELECT COUNT(*) AS `total` FROM `$table` WHERE `archived`=0;");
          $active = $gimpDB->getValue();

        $gimpDB->query("SELECT COUNT(*) AS `total` FROM `$table` WHERE `archived`=1;");
          $archived = $gimpDB->getValue();

        ?>
        <?php (!empty($rowCount) ? $rowCount : $rowCount = 0) ?>
        <tr class="<?= $rowCount%2?'row1':'row0' ?>" data-form_table="<?= $form['table'] ?>">
          <td NOWRAP><div><b><?= strtoupper($form['table']) ?></b></div>
            <a href="<?= Common::gimpLink('task=forms.edit&table='.$form['table']) ?>" title="Edit Form Settings">Edit Form</a>
          </td>
          <td><div><b><?= $form['name'] ?></b></div>
            <a href="<?= Common::gimpLink('task=forms.submissions&table='.$form['table']) ?>" title="View Form Submissions">View Submissions</a>
          </td>
          <td NOWRAP><?php echo (!empty($last) ? $last : '') ?></td>
          <td NOWRAP style="text-align:center;"><a href="<?= Common::gimpLink('task=forms.submissions&filter=new&table='.$form['table']) ?>" title="View New Submissions">[ <?php echo (isset($new) ? $new : '') ?> ]</a></td>
          <td NOWRAP style="text-align:center;"><a href="<?= Common::gimpLink('task=forms.submissions&archived=0&table='.$form['table']) ?>" title="View Active Submissions">[ <?php echo (isset($active) ? $active : '') ?> ]</a></td>
          <td NOWRAP style="text-align:center;"><a href="<?= Common::gimpLink('task=forms.submissions&archived=1&table='.$form['table']) ?>" title="View Archived Submissions">[ <?php echo (isset($archived) ? $archived : '') ?> ]</a></td>
        </tr>
        <?php
        $rowCount++;
      }
    ?>
  </tbody>
</table>
<?php include 'footer.php'; ?>