<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

$list_start = $this->app->request->getSessionRequest("{$task}_list_start", 'list_start', 0);
$list_limit = $this->app->request->getSessionRequest("{$task}_list_limit", 'list_limit', 20);
$archived   = $this->app->request->getSessionRequest("{$task}_archived", 'archived', null);
$ordering   = $this->app->request->getSessionRequest("{$task}_ordering", 'ordering', 'fg.created');
$filter     = $this->app->request->getSessionRequest("{$task}_filter", 'filter', null);
$keyword    = $this->app->request->getSessionRequest("{$task}_keyword", 'keyword', null);
$table      = $this->app->request->getSessionRequest("{$task}_table", 'table', null);

$gimpDB->query("
  SELECT COUNT(*) AS `total`
  FROM `#__wbfg_form_".$table."` AS `fg`
  WHERE 1 = 1
    ".( $archived != '' ? " AND (`fg`.`archived` = '{$archived}')" : '' )."
    ".( $keyword ? " AND (`fg`.`fullname` LIKE '%{$searchString}%' OR `fg`.`email` LIKE '%{$searchString}%')" : '' )."
    ".( $filter == 'read' ? " AND `fg`.`reviewed`!='0000-00-00 00:00:00'" : ($filter == 'unread' ? " AND `fg`.`reviewed`='0000-00-00 00:00:00'" : '') )."
  ");
$total = $gimpDB->getValue();

$list_limit = $list_limit >= 0 ? $list_limit : 20;
$list_start = $list_start >= 0 && ($list_start * $list_limit < $total) ? $list_start : 0;

if( preg_match('/^(\w+\.\w+)_(\w+)$/',$ordering,$matches) ) {
  $orderStr = $matches[1].' '.$matches[2];
}
else {
  $orderStr = 'fg.created DESC';
}

$searchString = $keyword ? $gimpDB->escape(preg_replace('/\s+/','%',$keyword)) : null;
$gimpDB->query("
  SELECT fg.*, COUNT(fga.id) AS total
  FROM `#__wbfg_form_".$table."` AS `fg`
  LEFT JOIN `#__wbfg_attachments` AS `fga` ON (`fga`.`table` = '".$table."' AND `fga`.`table_id` = `fg`.`id`)
  WHERE 1 = 1
    ".( $archived != '' ? " AND (`fg`.`archived` = '{$archived}')" : '' )."
    ".( $keyword ? " AND (`fg`.`fullname` LIKE '%{$searchString}%' OR `fg`.`email` LIKE '%{$searchString}%')" : '' )."
    ".( $filter == 'read' ? " AND `fg`.`reviewed`!='0000-00-00 00:00:00'" : ($filter == 'unread' ? " AND `fg`.`reviewed`='0000-00-00 00:00:00'" : '') )."
  GROUP BY fg.id
  ORDER BY $orderStr
  LIMIT $list_start, $list_limit
  ");
$submissions = $gimpDB->getRows();

if ($task == 'forms.submissions.export') {
  return;
}

$extra_hidden = '<input type=hidden name=table value='.$table.' />';
$extra_hidden .= '<input type=hidden name=csvexp value="0" />';
$extra_hidden .= '<input type=hidden name=ordering value="'.$ordering.'" />';
$extra_hidden .= '<input type=hidden name=no_html value="0" />';
$extra_fields = ''
  . '<input type="text" name="keyword" value="'.$keyword.'" onChange="document.fgNavForm.submit();" placeholder="Search..." /> '
  . '<select name="filter" onChange="document.fgNavForm.submit();">'
    . '<option value="">- Read/Unread -</option>'
    . '<option value="unread" '. ($filter == 'unread' ? 'selected' : '') .'>Unread</option>'
    . '<option value="read" '. ($filter == 'read' ? 'selected' : '') .'>Read</option>'
  . '</select> '
  . '<select name="archived" onChange="document.fgNavForm.submit();">'
    . '<option value="">- Active/Archived -</option>'
    . '<option value="0" '. ($archived == '0' ? 'selected' : '') .'>Active</option>'
    . '<option value="1" '. ($archived == '1' ? 'selected' : '') .'>Archived</option>'
  . '</select>'
  ;
?>
<?php include 'header.php'; ?>
<?php Form::showPageNav( $total, $list_start, $list_limit, $extra_hidden, $extra_fields, 'fgNavForm' ); ?>
<form action="<?php echo Common::gimpLink('task=forms.submissions&table='.$table); ?>" method="post" name="adminForm" id="adminForm">
  <table border=0 cellpadding=5 cellspacing=0 width=100% class=data>
    <colgroup>
      <col class="cb">
      <col class="id">
      <col class="fullname">
      <col class="email">
      <col class="created">
      <col class="reviewed">
      <col class="attachment_count">
    </colgroup>
    <thead>
      <tr>
        <th width=1%><input type="checkbox" name="toggle" value="" onclick="checkAll(this);" /></th>
        <th><a href="#" class="order" onClick="order('fg.id<?= $ordering == 'fg.id_asc' ? '_desc' : '_asc' ?>');" title="Order by ID">ID</a></th>
        <th><a href="#" class="order" onClick="order('fg.fullname<?= $ordering == 'fg.fullname_asc' ? '_desc' : '_asc' ?>');" title="Order by Full Name">Fullname</a></th>
        <th><a href="#" class="order" onClick="order('fg.email<?= $ordering == 'fg.email_asc' ? '_desc' : '_asc' ?>');" title="Order by Email Address">Email</a></th>
        <th><a href="#" class="order" onClick="order('fg.created<?= $ordering == 'fg.created_asc' ? '_desc' : '_asc' ?>');" title="Order by Date Created">Created</a></th>
        <th><a href="#" class="order" onClick="order('fg.reviewed<?= $ordering == 'fg.reviewed_asc' ? '_desc' : '_asc' ?>');" title="Order by Date Reviewed">Reviewed</a></th>
        <th># Attachments</th>
      </tr>
    </thead>
    <tbody>
      <?php
        $rowCount = 0;
        foreach( $submissions AS $submission ){
          $submission = (array)$submission;
          $row = new \stdClass(); $row->id = $submission['id'];
          $checked = "<input type='checkbox' name='cid[]' value='".$submission['id']."'>";
          $link = Common::gimpLink('task=forms.submissions.view&table='.$table.'&id='.$submission['id']);
          ?>
          <tr class="<?= ($rowCount%2?'row1':'row0') . ($submission['archived']?' archived':'') ?>" data-submission_id="<?= $submission['id'] ?>">
            <td align="center"><?php echo $checked; ?></td>
            <td><a href="<?= $link ?>"><?= strtoupper($table).$submission['id'] ?></a></td>
            <td><?= Form::htmlOut($submission['fullname']) ?></td>
            <td><?= Form::htmlOut($submission['email']) ?></td>
            <td><?= Form::htmlOut($submission['created']) ?></td>
            <td><?= Form::htmlOut($submission['reviewed']) ?></td>
            <td><?= Form::htmlOut($submission['total']) ?></td>
          </tr>
          <?php
          $rowCount++;
        }
      ?>
    </tbody>
  </table>
  <input type="hidden" name="table" value="<?= $table ?>" />
  <input type="hidden" name="task" value="" />
</form>
<script>
  function order( by ){
    document.fgNavForm.ordering.value = by;
    document.fgNavForm.submit();
  }
</script>
<?php include 'footer.php'; ?>
