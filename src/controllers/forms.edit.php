<?php namespace WebuddhaInc\FormGimp;

use \WebuddhaInc\Params;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

if( $_REQUEST['table'] ){
  $template_dir = $this->app->config->get('theme_path');
  $index        = Common::getIndexArray($this->app->request->request->get('table'));
  if (!$index) {
    $this->app->redirect('task=forms', 'Invalid Form');
  }
  $css_exists       = file_exists($template_dir.'/'.$index['file_css'].'.css');
  $form_exists      = file_exists($template_dir.'/'.'forms'.$index['file_form'].'.php');
  $processor_exists = file_exists($template_dir.'/'.$index['file_process'].'.php');
}
else {
  $index = array();
}

$this->app->mediaInclude( 'js', 'js/jquery.wbsortable.js' );
$this->app->mediaInclude( 'css', 'js/jquery.wbsortable.css' );

?>
<?php include 'header.php'; ?>
<form action="<?= Common::gimpLink('task=forms.edit') ?>" method=POST name="adminForm" id="adminForm">
<input type=hidden name=task value="forms.edit.save" />
<input type=hidden name=next value="forms" />
<input type=hidden name=id value="<?= $index['id'] ?>" />
  <div class=fgtbar>
    <?php
      if( $index['id'] ){
        ?>
        <h3>Editing: <?= $index['name']?$index['name']:' New Record' ?></h3>
        <?php
      } else {
        ?>
        <h3>This is a New Form Record</h3>
        <?php
      }
    ?>
  </div>
  <table border=0 cellpadding=5 cellspacing=0 width=100% class=adminform>
    <tr>
      <td width="70%">
        <table border=0 cellpadding=5 cellspacing=0 width=100%>
          <tr>
            <td><label for="name">Name:</label></td>
            <td><input style="width:90%" type=text name="name" value="<?= ($index['name']) ?>" /></td>
          </tr>
          <tr>
            <td><label for="desc">Desc:</label></td>
            <td><input style="width:90%" type=text name="desc" value="<?= ($index['desc']) ?>" /></td>
          </tr>
          <tr>
            <td><label for="table">Form ID:</label></td>
            <td>
              <?php if((int)$index['id']){ ?>
                <input style="width:100px" type=text value="<?= ($index['table']) ?>" disabled="true" />
                <input type=hidden name="table" value="<?= $index['table'] ?>" />
                <a class="btn btn-sm btn-default" href="<?= Common::gimpLink('task=form&table='.$index['table']) ?>" target="_blank">View Form</a>
              <?php } else { ?>
                <input style="width:100px" type=text name="table" value="<?= ($index['table']) ?>" />
              <?php } ?>
            </td>
          </tr>
          <tr>
            <td><label for="name">CSS File:</label></td>
            <td>
              <input style="width:100px" type=text name="file_css" value="<?= ($index['file_css']) ?>" />.css -
              <span><?= ($css_exists ? ' <font color=green>found</font>' : ($index['file_css'] ? ' <font color=red>not found</font>' : ' <font color=gray>use default.css</font>')) ?></span><br/>
              <span style="font-style:italic;"><?= $template_dir ?>/formgimp/css/</span>
            </td>
          </tr>
          <tr>
            <td><label for="name">Form File:</label></td>
            <td>
              <input style="width:100px" type=text name="file_form" value="<?= ($index['file_form']) ?>" />.php -
              <span><?= ($form_exists ? ' <font color=green>found</font>' : ($index['file_form'] ? ' <font color=red>not found</font>' : ' <font color=gray>auto generate</font>')) ?></span><br/>
              <span style="font-style:italic;"><?= $template_dir ?>/formgimp/forms/</span>
            </td>
          </tr>
          <tr>
            <td><label for="name">Form Processor:</label></td>
            <td>
              <input style="width:100px" type=text name="file_process" value="<?= ($index['file_process']) ?>" />.php -
              <span><?= ($processor_exists ? ' <font color=green>found</font>' : ($index['file_process'] ? ' <font color=red>not found</font>' : ' <font color=gray>no processor</font>')) ?></span><br/>
              <span style="font-style:italic;"><?= $template_dir ?>/formgimp/form_processors</span>
            </td>
          </tr>
          <tr>
            <td><label for="secure_img">Require Captcha:</label></td>
            <td>
              <select name="secure_img" style="width:90%;">
                <option value="1">Required</option>
                <option value="0" <?= $index['secure_img']==0 ? 'selected' : '' ?>>Not Required</option>
              </select>
            </td>
          </tr>
          <tr>
            <td><label for="redirect_url">Redirect URL:</label></td>
            <td><input style="width:90%;" type=text name="redirect_url" value="<?= ($index['redirect_url']) ?>" /></td>
          </tr>
          <tr>
            <td><label for="email_alerts">Alert Recipients:</label></td>
            <td>
              <textarea style="width:90%;height:90px;" name="email_alerts"><?php
                $email_alerts = (array)@$index['email_alerts'];
                if(is_array($email_alerts))
                  echo join("\n",$email_alerts);
              ?></textarea><br>
              <span style="font-weight:normal;font-style:italic;">List Email Addresses that will be sent alerts when a submission is completed.</span>
            </td>
          </tr>
          <tr>
            <td><label for="confirmation_msg">Confirmation Message:</label></td>
            <td>
              <textarea style="width:90%;height:90px;" name="confirmation_msg"><?php echo htmlspecialchars(@$index['confirmation_msg']); ?></textarea><br>
              <span style="font-weight:normal;font-style:italic;">This message will display after submission and in the confirmation email sent to the visitor.</span>
            </td>
          </tr>
        </table>
      </td>
      <td rowspan="3" valign="top" width="30%">
        <div class="paramList">
          <?php
          $xmlPath = Form::getAssetPath('form', $index['table'].'.xml');
          if (!$xmlPath) {
            $xmlPath = Form::getAssetPath('form', 'default.params.xml');
          }
          if ($xmlPath) {
            $fields = Common::xml_getFields($xmlPath);
            $fieldParams = new Params(array('field_key' => 'params'));
            echo Form::renderFieldset($fields, (array)@$index['params'], $fieldParams);
          }
          ?>
        </div>
      </td>
    <tr>
  </table>
  <table class="adminlist wbsortable" id="wbsortable">
    <thead>
      <tr>
        <th style="text-align:left;" width="1%">Show</th>
        <th style="text-align:left;" width="15%" nowrap>Type / Field Name</th>
        <th style="text-align:left;" width="35%" nowrap>Field Label / Tool Tip</th>
        <th style="text-align:left;">Options (one per line, key|label)</th>
        <th style="text-align:left;" width="10%" nowrap>Required / Validation</th>
        <th width="1%">&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <?php
        $formFields = (array)@$index['field_types'];

        if( !is_array($formFields) || !count($formFields) ) {
          $formFields = array(
            array('ordering'=>'1','name'=>'fullname','label'=>'Full Name','tooltip'=>'','visible'=>'1','required'=>'1','validate'=>'\w+','type'=>'text','options'=>''),
            array('ordering'=>'2','name'=>'email','label'=>'Email Address','tooltip'=>'','visible'=>'1','required'=>'1','validate'=>'[\w\.\-]+\@[\w\.\-]+','type'=>'text','options'=>''),
            array('ordering'=>'3','name'=>'phone','label'=>'Phone Number','tooltip'=>'','visible'=>'1','required'=>'1','validate'=>'\w+','type'=>'text','options'=>''),
            array('ordering'=>'4','name'=>'subject','label'=>'Message Subject','tooltip'=>'','visible'=>'1','required'=>'1','validate'=>'\w+','type'=>'text','options'=>''),
            array('ordering'=>'5','name'=>'description','label'=>'Message Description','tooltip'=>'','visible'=>'1','required'=>'1','validate'=>'\w+','type'=>'textarea','options'=>''),
            array('ordering'=>'6','name'=>'referrer','label'=>'Referred By','tooltip'=>'','visible'=>'1','required'=>'1','validate'=>'\w+','type'=>'text','options'=>'')
          );
        }

        $fieldCount = 0;
        for($i=0;$i<count($formFields);$i++){
          $field =& $formFields[$i];
          $fieldKey = 'field'.$i;
          ?>
          <tr class="wbs_row<?php echo ($i%2?1:0).' '.$field['type'].'-type'; ?>" id="field<?php echo $i ?>">
            <td align="center">
              <div><input type="checkbox" name="<?php echo $fieldKey ?>[visible]" value="1" <?php echo $field['visible']?'checked':'' ?> /></div>
            </td>
            <td>
              <div class="field-select">
                <select name="<?php echo $fieldKey ?>[type]">
                  <optgroup label="Standard">
                    <option <?php echo $field['type']=='text'?'selected':'' ?> value="text">Input Field</option>
                    <option <?php echo $field['type']=='textarea'?'selected':'' ?> value="textarea">Textarea Field</option>
                    <option <?php echo $field['type']=='list'?'selected':'' ?> value="list">Select List</option>
                    <option <?php echo $field['type']=='radio'?'selected':'' ?> value="radio">Radio List</option>
                    <option <?php echo $field['type']=='checkbox'?'selected':'' ?> value="checkbox">Checkbox</option>
                  </optgroup>
                  <optgroup label="Extended">
                    <option <?php echo $field['type']=='label'?'selected':'' ?> value="label">Text Block</option>
                    <option <?php echo $field['type']=='file'?'selected':'' ?> value="file">File Upload</option>
                    <option <?php echo $field['type']=='hidden'?'selected':'' ?> value="hidden">Hidden Value</option>
                    <option <?php echo $field['type']=='number'?'selected':'' ?> value="number">Number Field</option>
                    <option <?php echo $field['type']=='password'?'selected':'' ?> value="password">Password Field</option>
                    <option <?php echo $field['type']=='date'?'selected':'' ?> value="date">Date Field</option>
                    <option <?php echo $field['type']=='time'?'selected':'' ?> value="time">Time Field</option>
                    <option <?php echo $field['type']=='datetime-local'?'selected':'' ?> value="datetime-local">Date/Time Field</option>
                  </optgroup>
                </select>
              </div>
              <div class="field-text">
                <?php if(in_array($field['name'],Common::$coreFormFields)){ ?>
                  <input type="text" value="<?php echo Form::paramOut($field['name'])?>" disabled="true" />
                  <input type="hidden" name="<?php echo $fieldKey ?>[name]" value="<?php echo Form::paramOut($field['name'])?>" />
                <?php } else { ?>
                  <input type="text" name="<?php echo $fieldKey ?>[name]" value="<?php echo Form::paramOut($field['name'])?>" />
                <?php } ?>
              </div>
            </td>
            <td>
              <div class="field-text">
                <input type="text" name="<?php echo $fieldKey ?>[label]" value="<?php echo Form::paramOut($field['label']) ?>" />
              </div>
              <div class="field-text">
                <input type="text" name="<?php echo $fieldKey ?>[tooltip]" value="<?php echo Form::paramOut($field['tooltip']) ?>" />
              </div>
            </td>
            <td>
              <div class="field-textarea">
                <textarea name="<?php echo $fieldKey ?>[options]"><?php echo Form::htmlOut($field['options']) ?></textarea>
              </div>
            </td>
            <td>
              <div class="field-checkbox">
                <input type="checkbox" name="<?php echo $fieldKey ?>[required]" value="1" <?php echo $field['required']?'checked':'' ?> />
              </div>
              <div class="field-text">
                <input type="text" name="<?php echo $fieldKey ?>[validate]" value="<?php echo $field['validate'] ?>" />
              </div>
            </td>
            <td class="control">
              <?php if(in_array($field['name'],Common::$coreFormFields)){ ?>
                &nbsp;
              <?php } else { ?>
                <button type="button" class="btn btn-sm btn-danger" onclick="return formGimp.forms.edit.delField(this);"> Delete </button>
              <?php } ?>
            </td>
          </tr>
          <?php
          $fieldCount++;
        }
      ?>
    </tbody>
    <tfoot>
      <tr>
        <td class="control bottom" colspan="6">
          <button onclick="return formGimp.forms.edit.newField();" type="button" class="btn btn-primary">  + Add New Field  </button>
          <button onclick="submitForm('forms.edit.apply');" type="button" class="btn btn-success"> Apply Changes </button>
        </td>
      </tr>
    </tfoot>
  </table>
</form>

<?php if( (int)$index['id'] ){ ?>
<div class="sampleForm">
  <h1>Sample Form</h1>
  <div><?= Form::render( $index, array('render_form' => false) ); ?></div>
</div>
<?php } ?>

<script>
  var fieldCount = <?= $fieldCount ?>;
  var formValidate = [
    ['name',/\w+/,'Please Provide a Form Name'],
    ['table',/\w+/,'Please Provide a Form Id']
    ];
  jQuery(document).ready(function(){
    if( typeof wbSortable != 'undefined' ){
      function wbSortablePostFunc(){};
      var mySortable = new wbSortable('wbsortable', 'wbSortablePostFunc', {'ghost':false});
    }
  });
</script>
<?php include 'footer.php'; ?>
