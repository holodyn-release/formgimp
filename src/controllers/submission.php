<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

list($table, $submission_id) = explode(',',Common::aes_decrypt($this->app->request->request->get('xid')));

// Validate
if (empty($table) || empty($submission_id)) {
  $this->app->redirect('task=forms', 'Invalid Request');
}

// Load Index
$index = Common::getIndexArray($table);
if (!$index) {
  $this->app->redirect('task=forms', 'Invalid Request');
}

// Load Submission
$gimpDB->query("
  SELECT *
  FROM `#__wbfg_form_".$index['table']."`
  WHERE `id`='". (int)$submission_id ."'
  ");
$submission = $gimpDB->getRow();
if (!$submission) {
  $this->app->redirect('task=forms.sumissions', 'Invalid Request');
}
$submission = (array)$submission;

?>
<?php include 'header.php'; ?>
<div class="fgViewSubmission">
  <h1><?= $index['name'] ?> form submission on <?= $submission['created'] ?></h1>
  <table border=0 cellpadding=2 cellspacing=0 width=100%>
    <tr>
      <th NOWRAP>Full Name:</th>
      <td><?= $submission['fullname'] ?></td>
    </tr>
    <?php if ($submission['email']) { ?>
    <tr>
      <th NOWRAP>Email:</th>
      <td><a href="mailto:<?= $submission['fullname'] ?> (<?= $submission['email'] ?>)" title="Send Email"><?= $submission['email'] ?></a></td>
    </tr>
    <?php } ?>
    <?php if ($submission['phone']) { ?>
    <tr>
      <th NOWRAP>Phone:</th>
      <td><?= $submission['phone'] ?></td>
    </tr>
    <?php } ?>
    <?php if ($submission['referrer']) { ?>
    <tr>
      <th NOWRAP>Referrer:</th>
      <td><?= $submission['referrer'] ?></td>
    </tr>
    <?php } ?>
    <?php if ($submission['subject']) { ?>
    <tr>
      <th NOWRAP>Subject:</th>
      <td><?= $submission['subject'] ?></td>
    </tr>
    <?php } ?>
    <?php if ($submission['description']) { ?>
    <tr>
      <th NOWRAP>Description:</th>
      <td><pre class=description><?= $submission['description'] ?></pre></td>
    </tr>
    <?php } ?>
    <?php
      // Print Additional Fields
      $fields = $gimpDB->getTableFields($index['table']);
      foreach( $fields AS $field ) {
        if( !in_array($field, Common::$coreFormFields) ){
          ?>
          <tr>
            <th valign=top><?= $index['field_titles'][$field] ?>:</th>
            <td valign=top><?php
              if( preg_match('/^Array/', $submission[ $field ] ) )
                echo join("<br/>",eval( 'return '.$submission[ $field ].';' ));
              else
                echo $submission[ $field ];
            ?></td>
          </tr>
          <?php
        }
      }
      $formRequired = Array();
      $formFields = unserialize($index['field_types']);
      if( !is_array($formFields) )
        $formFields = array();
      $values = unserialize($submission['data']);
      if( !is_array($values) ) $value = array();
      foreach( $formFields AS $field ){
        $field_id    = $field['name'];
        $field_label = $field['label'];
        if( !in_array($field['type'],array('label','file')) && $field_id && !in_array($field_id, Common::$coreFormFields) ){
          $field_label = $field_label ? $field_label : ucwords(preg_replace('/\_/',' ',$field_id));
          if( array_key_exists($field_id,$formRequired) )
            $field_label .= ' *';
          ?>
          <tr>
            <th valign=top><?= $field_label ?>:</th>
            <td><?php
              if (is_array($values[ $field_id ]))
                echo join('<br/>',$values[ $field_id ]);
              else if(strpos($values[ $field_id ], '[other]') !== false && isset($values[ $field_id . '_other_' ]))
                echo str_replace('[other]', $values[ $field_id . '_other_' ], $values[ $field_id ]);
              else
                echo $values[ $field_id ];
              ?></td>
          </tr>
          <?php
        }
      }
      $gimpDB->query("
        SELECT *
        FROM `#__wbfg_attachments`
        WHERE `table`='".$index['table']."'
          AND `table_id`='".$submission['id']."'
        ORDER BY `field`, `filename`
          , `size` DESC
        ");
      $attachments = $gimpDB->getRows(); $rowCount = 1;
      if ($attachments) {
        ?>
        <tr>
          <th NOWRAP>Attachments:</th>
          <td>
            <table border=0 cellpadding=0 cellspacing=0 width=100% class=attachments>
              <?php
                foreach( $attachments AS $attachment ){
                  $attachment = (array)$attachment;
                  $fileName = $attachment['filename'];
                  $download_url = $this->app->route('task=download&format=raw&eid='.Common::aes_encrypt($attachment['id']));
                  $thumbnail_url = $this->app->route('task=thumbnail&format=raw&eid='.Common::aes_encrypt($attachment['id']));
                  if( strlen($fileName) > 48 ) $fileName = preg_replace('/^(.{48}).*$/','$1',$fileName).'...';
                  echo '<tr>';
                  echo '<td>'.$rowCount++.') </td>';
                  echo '<td>'.$attachment['field'].'</td>';
                  echo '<td><a href="'. $download_url .'" title="'.$attachment['filename'].'">'.$fileName.'</a></td>';
                  echo '<td>'.Common::formatFileSize($attachment['size']).'</td>';
                  if( preg_match('/(jpg|gif|png|bmp)$/', strtolower($attachment['filename']), $match) ){
                    ?>
                    <td>
                      <img src="<?= $thumbnail_url ?>" style="max-width: 320px; max-height: 240px;" />
                    </td>
                    <?php
                  }
                  else if( preg_match('/(mp4|flv|mpeg|vob|ogg|ogv|avi|mov|qt|wmv|m4v)$/', strtolower($attachment['filename']), $match) ){
                    ?>
                    <td>
                      <video id="sampleMovie" width="320" controls>
                        <source src="<?= $thumbnail_url ?>" />
                      </video>
                    </td>
                    <?php
                  }
                  else
                    echo '<td>&nbsp;</td>';
                  echo '</tr>';
                }
              ?>
            </table>
          </td>
        </tr>
        <?php
      }
    ?>
    <tr>
      <th NOWRAP>Reviewed:</th>
      <td><?= $submission['reviewed'] ?></td>
    </tr>
    <tr>
      <th NOWRAP>Archived:</th>
      <td><?= $submission['archived'] ? 'YES' : 'NO' ?></td>
    </tr>
  </table>
</div>
<?php include 'footer.php'; ?>
