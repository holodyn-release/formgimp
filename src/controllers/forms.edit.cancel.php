<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

$this->app->redirect('task=forms', 'Action Cancelled');