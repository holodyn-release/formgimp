<?php namespace WebuddhaInc\FormGimp;

// no direct access
defined('FORMGIMP') or die( 'Restricted access' );

global $fg_email_from, $fg_auto_archive, $fg_archive_timeout, $fg_confirmation_msg, $fg_recaptcha_key, $fg_recaptcha_secret;

?>
<?php include 'header.php'; ?>
<form action="<?php echo Common::gimpLink('task=setup.save'); ?>" method="post" name="adminForm" id="adminForm">
<input type=hidden name=task value="setup.save" />
  <div class=fgtbar>
    <h3>Manage System Settings</h3>
  </div>
  <table border=0 cellpadding=5 cellspacing=0 width=100% class=data>
    <tr>
      <th width=1% NOWRAP>System Sender: </td>
      <td width=1%><input type=text name=email_from value="<?= $fg_email_from ?>" size="40" /></td>
      <th>The email address used when sending confirmation / alert emails.</th>
    </tr>
    <tr>
      <th width=1% NOWRAP>Auto Archive: </td>
      <td width=1%><select name=auto_archive><option value=0>NO</option><option value=1 <?= ($fg_auto_archive ? 'selected' : '') ?>>YES</option></select></td>
      <th>Automatically Archive Items older than the Archive Timeout value.</th>
    </tr>
    <tr>
      <th width=1% NOWRAP>Archive Timeout: </td>
      <td width=1%><input type=text name=archive_timeout value="<?= $fg_archive_timeout ?>" size="5" /></td>
      <th>The number of days that an item can exist before it will be forced into an archived state.</th>
    </tr>
    <tr>
      <th width=1% NOWRAP>Google ReCaptcha Key: </td>
      <td width=1%><input type=text name=recaptcha_key value="<?= $fg_recaptcha_key ?>" size="40" /></td>
      <th>Site Key provided to you by Google for the reCaptcha library.</th>
    </tr>
    <tr>
      <th width=1% NOWRAP>Google ReCaptcha Secret: </td>
      <td width=1%><input type=text name=recaptcha_secret value="<?= $fg_recaptcha_secret ?>" size="40" /></td>
      <th>Secret Key provided to you byu Google for the reCaptcha library.</th>
    </tr>
    <tr><th colspan=3>Confirmation Message: <i>This message will display after submission and in the confirmation email sent to the visitor.</i></th></tr>
    <tr><td colspan=3><textarea name=confirmation_msg style="width:90%; height:140px;"><?= $fg_confirmation_msg ?></textarea></td</tr>
  </table>
</form>
<script>
  var formValidate = [
    ['archive_timeout',/\w+/,'Please Provide a Archive Timeout'],
    ['email_from',/\w+/,'Please Provide a Email Address']
    ];
</script>
<?php include 'footer.php'; ?>
