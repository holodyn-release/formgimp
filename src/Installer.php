<?php

namespace WebuddhaInc\FormGimp;

class Installer {

  public $app;

  function __construct( $app ){
    $this->app = $app;
  }

  function getCurrentVersion(){
    $xml = new \SimpleXMLElement(file_get_contents(__DIR__ . '/app.xml'));
    if ($version = $xml->xpath('/app/version')) {
      return (string)reset($version);
    }
    return 0;
  }

  function getInstalledVersion(){
    $this->app->db->query("SHOW TABLES LIKE '#__wbfg_setup'");
    if ($this->app->db->getRows()) {
      $this->app->db->query("SELECT `value` FROM `#__wbfg_setup` WHERE `name` = 'version'");
      if ($row = $this->app->db->getRow()) {
        return $row->value;
      }
    }
    return 0;
  }

  function setInstalledVersion($version){
    $this->app->db->query("
      UPDATE `#__wbfg_setup`
      SET `value` = '". $this->app->db->escape($version) ."'
      WHERE `name` = 'version'
      ");
  }

  function checkSchema(){
    $current_version   = $this->getCurrentVersion();
    $installed_version = $this->getInstalledVersion();
    if (version_compare($current_version, $installed_version, '>')) {
      $this->install($current_version, $installed_version);
    }
  }

  function install($current_version, $installed_version){
    if (version_compare($installed_version, '1.0.0', '<=')) {
      $this->installSqlFile(__DIR__.'/sql/install.mysql.utf8.sql');
      $this->setInstalledVersion('1.0.0');
    }
    $dh = opendir(__DIR__.'/sql/updates');
    while (false !== ($file = readdir($dh))) {
      if (!preg_match('/^\.+$/',$file)) {
        $file_version = preg_replace('/[^0-9\.]/','',preg_replace('/\.sql$/','',$file));
        if (version_compare($installed_version, $file_version, '<')) {
          $this->installSqlFile(__DIR__.'/sql/updates/'.$file);
          $this->setInstalledVersion($file_version);
        }
      }
    }
  }

  function uninstall(){
    $this->installSqlFile(__DIR__.'/sql/uninstall.mysql.utf8.sql');
  }

  function installSqlFile($file){
    $sql   = file_get_contents($file);
    $lines = explode("\n", $sql);
    $query = array();
    foreach ($lines AS $line) {
      if (preg_match('/^(\s*\-|\s*$)/',$line)) {
        continue;
      }
      $query[] = $line;
      if (preg_match('/\;\s*$/',$line)) {
        $this->app->db->query(implode(' ',$query));
        $query = array();
      }
    }
  }

}

/**

// Add Table if not exists
  $this->db->query("show tables like '#__wbfg_captcha_session'");
  if( !count($this->db->getRows()) ){
    $this->db->query( "
      CREATE TABLE `#__wbfg_captcha_session` (
        `session_id` varchar(200) NOT NULL default '',
        `key` varchar(32) NOT NULL default '',
        `created` varchar(14) NOT NULL default '',
        PRIMARY KEY  (`session_id`)
      )
      COMMENT='secureImgCreator - CrossRef';
      ");
  }

 */