<?php

namespace WebuddhaInc\FormGimp;

class Controller {

  public $app;

  public function __construct(App $app){
    $this->app = $app;
  }

  public function render($task){

    /**
     * Legacy
     */
    $gimpDB = $GLOBALS['gimpDB'];
    $gimpDB->query("SELECT * FROM #__wbfg_setup ORDER BY name;");
    $_setup = array();
    foreach ($gimpDB->getRows() AS $setup)
      $_setup['fg_'.$setup->name] = $setup->value;
    extract($_setup);
    unset($_setup);

    /**
     * Lookup & Load
     * @var [type]
     */
    $path = __DIR__ . '/controllers/';
    $file = preg_replace('/[^A-Za-z0-9]/', '', $task);
    if ($task && is_readable($path . $task . '.php')) {
      ob_start();
      if ($this->app->request->request->get('format', 'html') == 'html')
        echo '<div class="formgimp controller '.preg_replace('/_+/', '_', preg_replace('/[^A-Za-z0-9\-\_]/', '_', $task)).'">';
      try {
        require $path . $task . '.php';
      } catch (Exception $e) {
        $this->app->mediaInclude('css', 'css/default.css');
        $this->app->renderMsg('error', $e->getMessage());
      }
      if ($this->app->request->request->get('format', 'html') == 'html')
        echo '</div>';
      return ob_get_clean();
    }

    return 'Invalid Task: ' . $task;
  }

}