<?php

namespace WebuddhaInc\FormGimp;

use \WebuddhaInc\Params;

class Form {

  // ************************************************************************************************************************************************************
  public static function makeOption( $value, $label=null, $vKey='value', $lKey='name' ){
    $opt = new \stdClass();
    $opt->{$vKey} = $value;
    $opt->{$lKey} = is_null($label) ? $value : $label;
    return $opt;
  }

  // ************************************************************************************************************************************************************
  public static function paramOut( $value, $def = '' ){
    if( is_null($value) || !strlen(trim($value)) )
      return $def;
    return htmlspecialchars($value);
  }

  // ************************************************************************************************************************************************************
  public static function htmlOut( $value, $def = '' ){
    if( is_null($value) || !strlen(trim($value)) )
      return $def;
    return htmlspecialchars($value);
  }

  // ************************************************************************************************************************************************************
  public static function cleanId( $str ){
    $str = preg_replace('/[^\w\d\_]+/','_',$str);
    $str = preg_replace('/^_+|_+$/','',$str);
    return $str;
  }

  // ************************************************************************************************************************************************************
  public static function getAssetUrl($type, $file){
    $asset = Common::$app->mediaLookup( $type, $file );
    if ($asset) return $asset->url;
  }
  public static function getAssetPath($type, $file){
    $asset = Common::$app->mediaLookup( $type, $file );
    if ($asset) return $asset->path;
  }

  // ************************************************************************************************************************************************************
  public static function getFieldParams(){
    $params = new Params(array(
      'class'       => null,
      'default'     => null,
      'disabled'    => null,
      'extra'       => null,
      'id'          => null,
      'label'       => null,
      'maxlength'   => null,
      'multiple'    => null,
      'name'        => null,
      'options'     => null,
      'placeholder' => null,
      'required'    => null,
      'style'       => null,
      'toolip'      => null,
      'tooltip'     => null,
      'type'        => null,
      'validate'    => null,
      'visible'     => null,
      'value'       => null,
      'events'      => null,
      ));
    foreach (func_get_args() AS $arg) {
      if (is_array($arg) || is_object($arg)) {
        $params->merge($arg);
      }
    }
    return $params;
  }

  // ************************************************************************************************************************************************************
  public static function render( &$index, $params=null ){
    $media_path  = Common::$app->mediaPath();
    $media_root  = Common::$app->mediaRoot();
    $theme_path  = Common::$app->config->get('theme_path');
    $theme_root  = Common::$app->config->get('theme_root');
    $theme_ready = $theme_path && $theme_root && is_dir($theme_path);
    $params      = new Params(array(
      'use_tooltip'    => false,
      'use_wbform'     => false,
      'load_media'     => true,
      'render_form'    => true,
      'render_fields'  => true,
      'render_captcha' => true,
      'render_custom'  => false
      ),
      Common::xml_getFieldDefaults(Form::getAssetPath('form', 'default.params.xml')),
      (array)$index['params'],
      $params
      );
    $html        = array();

    // -------------------------------------------------------------------------
    //                                                                Load Media
    // -------------------------------------------------------------------------
    //

      if ($params->get('load_media')) {

        // Prepare to collect
          $incs = array(
            'css' => array(),
            'js'  => array()
            );

        // JS Core / UI
          Common::$app->mediaInclude( 'js', 'js/jquery.js' );
          if ($params->get('use_wbform')) {
            Common::$app->mediaInclude( 'js', 'js/jquery.wbform.js' );
            Common::$app->mediaInclude( 'css', 'js/jquery.wbform.css' );
          }
          if ($params->get('use_tooltip')) {
            Common::$app->mediaInclude( 'js', 'js/jquery.ui.core.js' );
            Common::$app->mediaInclude( 'js', 'js/jquery.ui.tooltip.js' );
          }

        // JS
          $js = Form::getAssetUrl('js', $index['table'] . '.js');
          if (!$js)
            $js = Form::getAssetUrl('js', 'default.js');
          if ($js)
            $incs['js'][] = $js;

        // CSS
          $css = Form::getAssetUrl('css', $index['file_css'] . '.css');
          if (!$css)
            $css = Form::getAssetUrl('css', $index['table'] . '.css');
          if (!$css)
            $css = Form::getAssetUrl('css', 'default.css');
          if ($css)
            $incs['css'][] = $css;

        // Apply Includes
          foreach($incs['css'] AS $inc)
            Common::$app->mediaInclude('css', $inc);
          foreach($incs['js'] AS $inc)
            Common::$app->mediaInclude('js', $inc);

      }

    // -------------------------------------------------------------------------
    //                                                    TODO: Load Custom Form
    // -------------------------------------------------------------------------

      if ($params->get('render_custom', 1)) {
        $_customPath = Form::getAssetPath('form', $index['file_form'] . '.php');
        if (!$_customPath)
          $_customPath = Form::getAssetPath('form', $index['table'] . '.php');
        if ($_customPath) {
          ob_start();
          include $_customPath;
          return ob_get_clean();
        }
      }

    // -------------------------------------------------------------------------
    //                                                         Start HTML Output
    // -------------------------------------------------------------------------

      $html[] = '<fieldset class="formgimp" id="'.$index['table'].'">';
      if ($params->get('show_title')) {
        $html[] = '<legend>'. ($params->get('title') ?: $index['name']) .'</legend>';
      }
      if (Common::$app->request->request->get('message')) {
        $html[] = '<div id="formGimp_form_alert"><h3>'.Common::$app->request->request->get('message').'</h3></div>';
      }
      if ($params->get('render_form')) {
        $html[] = '<form name="formGimp_form" id="formGimp_form" method="post" enctype="multipart/form-data">';
        $html[] = '<input type="hidden" name="page" value="formgimp" />';
        $html[] = '<input type="hidden" name="task" value="form.submit" />';
        $html[] = '<input type="hidden" name="form_id" value="'.$index['table'].'" />';
        $html[] = '<input type="hidden" name="return" value="'.Form::paramOut($_SERVER['REQUEST_URI']).'" />';
      }

    // -------------------------------------------------------------------------
    //                                                               Form Fields
    // -------------------------------------------------------------------------

      if ($params->get('render_fields')) {
        $formFields      = (array)@$index['field_types'];
        $formDataCache   = Common::loadTableCache( session_id(), $index['table']);
        $formDataRequest = Common::$app->request->request->get('field', array());
        $formData        = array_merge($formDataCache, $formDataRequest);
        $html[] = Form::renderFieldset( $formFields, $formData, $params );
      }

    // -------------------------------------------------------------------------
    //                                                      Secure Img/ReCaptcha
    // -------------------------------------------------------------------------
      if ($params->get('render_captcha') && @$index['secure_img']) {
        if (class_exists('\ReCaptcha\ReCaptcha') && $GLOBALS['fg_recaptcha_key'] && $GLOBALS['fg_recaptcha_secret']) {
          $html[] = '<script src="//www.google.com/recaptcha/api.js" async defer></script>';
          $html[] = '<div class="g-recaptcha" data-sitekey="'.$GLOBALS['fg_recaptcha_key'].'"></div>';
        }
        else {
          $html[] = '<div class="field field-captcha">';
          $html[] = Captcha::getIntance(Common::$app)->getCode();
          $html[] = '</div>';
        }
      }

    // --------------------------------------------------------------------------------------------
    //                                                                                       Submit
    // --------------------------------------------------------------------------------------------

      if ($params->get('render_form')) {
        if ($params->get('submit_type') == 'button') {
          $html[] = '<div class="submit"><button class="btn btn-primary"><span>'.Form::htmlOut($params->get('submit_label')).'</span></button></div>';
        }
        else {
          $html[] = '<div class="submit"><input class="btn btn-primary" type="submit" value="'.Form::paramOut($params->get('submit_label')).'" /></div>';
        }
        $html[] = '</form>';
      }

    // --------------------------------------------------------------------------------------------

      if ($params->get('use_wbform')) {
        $html[] = '<script language="javascript" type="text/javascript">
          jQuery(document).ready(function(){
            jQuery(\'formGimp_form\').wbForm();
          });
        </script>';
      }

    // --------------------------------------------------------------------------------------------
    //                                                                                  Return Form
    // --------------------------------------------------------------------------------------------

      $html[] = '</fieldset>';
      return implode(" ", $html)."\n";

  }

  // ************************************************************************************************************************************************************
  public static function renderFieldset( $fields, $values, $params=array() ){
    $html = array();
    foreach( $fields AS $field ){
      if (is_a($field, 'SimpleXMLElement')){
        $field_attributes = array();
        foreach($field->attributes() AS $key => $val){
          $field_attributes[(string)$key] = (string)$val;
        }
        if (in_array($field_attributes['type'], array('list', 'radio', 'checkbox'))) {
          foreach($field AS $key => $child){
            if ($key == 'option') {
              if (empty($field_attributes['options']))
                $field_attributes['options'] = array();
              $field_value = (string)$child->attributes()['value'];
              $field_attributes['options'][strlen($field_value) ? $field_value : (string)$child] = (string)$child;
            }
          }
        }
        $field = $field_attributes;
      }
      if (!isset($field['visible']) || $field['visible']) {
        $field = array_merge(array(
          'type'        => null,
          'name'        => null,
          'options'     => null,
          'label'       => null,
          'placeholder' => null,
          'required'    => null,
          'validate'    => null,
          'toolip'      => null,
          'default'     => null
          ), (array)$field);
        $options = array();
        switch( $field['type'] ){
          case 'text':
          case 'textarea':
          case 'label':
          case 'hidden':
            if( !isset($values[$field['name']]) && strlen(trim($field['options'])) )
              $values[$field['name']] = trim($field['options']);
            break;
          case 'list':
          case 'radio':
          case 'checkbox':
            if (is_string($field['options'])) {
              $set = explode("\n",preg_replace('/\r/','',$field['options']));
              foreach( $set AS $key ){
                $optSet = explode('|',$key,2);
                if( count($optSet) == 2 )
                  list( $value, $label ) = $optSet;
                else
                  $value = $label = $key;
                if( strlen($value) || strlen($label) )
                  $options[] = Form::makeOption( trim($value), trim($label), 'value', 'name' );
              }
            }
            else {
              foreach($field['options'] AS $key => $val) {
                $options[] = Form::makeOption( trim($key), trim($val), 'value', 'name' );
              }
            }
            if(!count($options)){
              $options[] = Form::makeOption( '0', 'No', 'value', 'name' );
              $options[] = Form::makeOption( '1', 'Yes', 'value', 'name' );
            }
          break;
        }
        $label = trim($field['label']);
        $field_params = array(
            'placeholder' => @$field['placeholder'],
            'required'    => @$field['required'],
            'validate'    => @$field['validate'],
            'tooltip'     => ($params->get('use_tooltip') ? @$field['tooltip'] : null),
            'span'        => ($params->get('use_tooltip') ? null : @$field['tooltip']),
            'options'     => &$options,
            'label'       => $label
            );
        $field_value = isset($values[$field['name']]) ? $values[$field['name']] : @$field['default'];
        switch( $field['type'] ){
          case 'text':
          case 'number':
          case 'password':
          case 'date':
          case 'time':
            $field_params['type'] = $field['type'];
            $html[] = Form::getInputField( $label, $params->get('field_key', 'field').'['.$field['name'].']', $field_value, $field_params );
            break;
          case 'datetime':
          case 'datetime-local':
            $field_params['type'] = 'datetime-local';
            $field_params['postfield'] = 'datetime-local';
            $html[] = Form::getInputField( $label, $params->get('field_key', 'field').'['.$field['name'].']', $field_value, $field_params );
            break;
          case 'textarea':
            $html[] = Form::getTextAreaField( $label, $params->get('field_key', 'field').'['.$field['name'].']', $field_value, $field_params );
            break;
          case 'list':
            $html[] = Form::getSelectField( $label, $params->get('field_key', 'field').'['.$field['name'].']', $field_value, $field_params );
            break;
          case 'radio':
            $html[] = Form::getRadioField( $label, $params->get('field_key', 'field').'['.$field['name'].']', $field_value, $field_params );
            break;
          case 'file':
            $field_params['type'] = 'file';
            $html[] = Form::getInputField( $label, $field['name'], $field_value, $field_params );
            break;
          case 'checkbox':
            $html[] = Form::getCheckboxField( $label, $params->get('field_key', 'field').'['.$field['name'].']', $field_value, $field_params );
            break;
          case 'hidden':
            $html[] = Form::getHiddenField( $label, $params->get('field_key', 'field').'['.$field['name'].']', $field_value, $field_params );
            break;
          default:
          case 'label':
            $html[] = Form::getTextField( $label, $params->get('field_key', 'field').'['.$field['name'].']', $field_value, $field_params );
            break;
        }
      }
    }
    return implode(" ", $html)."\n";
  }

  // ************************************************************************************************************************************************************
  public static function getToolTip( $title, $tooltip ){
    return $title;
    return '<a title="'.Form::paramOut($title).'" class="tooltip">'.$title.'</a>';
  }

  // ************************************************************************************************************************************************************
  public static function drawTextField( $field_label, $field_id=null, $value=null, $params=Array() ){
    echo Form::getTextField($field_label, $field_id, $value, $params);
  }
  public static function getTextField( $field_label, $field_id=null, $value=null, $params=Array() ){
    $params = (array)Form::getFieldParams($params, array('id' => Form::cleanId($field_id), 'name' => $field_id, 'label' => $field_label));
    $html = array();
    $html[] = '<div class="field field-txtlbl '.Form::cleanId($field_id).'">';
    $html[] = '<label for="'.Form::cleanId($field_id).'">'.($params['tooltip']?Form::getToolTip($field_label,$params['tooltip']):$field_label).'</label>';
    $html[] = '<span>'.Form::htmlOut( $value ).'</span>';
    $html[] = '</div>';
    return implode(" ", $html)."\n";
  } // drawTextField

  // ************************************************************************************************************************************************************
  public static function drawHiddenField( $field_label, $field_id, $value=null, $params=Array() ){
    echo Form::getHiddenField( $field_label, $field_id, $value, $params );
  }
  public static function getHiddenField( $field_label, $field_id, $value=null, $params=Array() ){
    $params = (array)Form::getFieldParams($params, array('id' => Form::cleanId($field_id), 'name' => $field_id, 'label' => $field_label));
    $params['type'] = 'hidden';
    return Form::getInputField( $field_label, $field_id, $value, $params );
  } // drawHiddenField

  // ************************************************************************************************************************************************************
  public static function drawInputField( $field_label, $field_id, $value=null, $params=Array() ){
    echo Form::getInputField( $field_label, $field_id, $value, $params );
  }
  public static function getInputField( $field_label, $field_id, $value=null, $params=Array() ){
    $html = array();
    if( !isset($params['type']) )
      $params['type'] = 'text';
    if( $params['type'] != 'hidden' ){
      $html[] = '<div class="field field-'.($params['type']?$params['type']:'text').' '.Form::cleanId($field_id).'">';
      $html[] = '<label for="'.Form::cleanId($field_id).'">'
              . ($params['tooltip']?Form::getToolTip($field_label,$params['tooltip']):$field_label)
              . ($params['required']?' *':'')
              . '</label>';
    }
    $html[] = '<input type="'.($params['type']?$params['type']:'text').'" '
            . ' name="'.$field_id.'"'
            . ' id="'. Form::cleanId($field_id) .'"'
            . ' value="'.Form::paramOut($value).'"'
            . ' placeholder="'.Form::paramOut($params['placeholder']).'"'
            . ($params['required']?' required="1"':'')
            . ' label="'.$field_label.'"'
            . ' validate="'.Form::paramOut($params['validate']).'"'
            . (!empty($params['tooltip']) ?' title="'.Form::paramOut($field_label).'" tooltip="'.Form::paramOut($params['tooltip']).'"':'')
            . (!empty($params['extra']) ? $params['extra'] : '')
            . (!empty($params['disabled']) ? ' disabled="true"':'')
            . (!empty($params['maxlength']) ? ' maxlength="'.$params['maxlength'].'"':'')
            ;
    foreach($params AS $k=>$v)
      if(preg_match('/^on\w+/',$k))
        $html[] = $k.'="'.$v.'" ';
    $html[] = ' />';
    if( $params['type'] != 'hidden' ){
      if($params['span'])
        $html[] = '<span>'.$params['span'].'</span>';
      $html[] = '</div>';
    }
    return implode(" ", $html)."\n";
  } // drawInputField

  // ************************************************************************************************************************************************************
  public static function drawTextAreaField( $field_label, $field_id, $value=null, $params=Array() ){
    echo Form::getTextAreaField( $field_label, $field_id, $value, $params );
  }
  public static function getTextAreaField( $field_label, $field_id, $value=null, $params=Array() ){
    $params = (array)Form::getFieldParams($params, array('id' => Form::cleanId($field_id), 'name' => $field_id, 'label' => $field_label));
    $html = array();
    $html[] = '<div class="field field-textarea '.Form::cleanId($field_id).'">';
    $html[] = '<label for="'.Form::cleanId($field_id).'">'
            . (!empty($params['tooltip']) ? Form::getToolTip($field_label,$params['tooltip']) : $field_label)
            . (!empty($params['required']) ?' *':'')
            . '</label>';
    $html[] = '<textarea name="'.$field_id.'" id="'. Form::cleanId($field_id) .'" '.(!empty($params['extra']) ? $params['extra'] : '').' '
            . (!empty($params['tooltip'])?' title="'.Form::paramOut($field_label).'" tooltip="'.Form::paramOut($params['tooltip']).'"':'')
            . (!empty($params['maxlength'])?' maxlength="'.$params['maxlength'].'"':'')
            . (!empty($params['placeholder'])?' placeholder="'.$params['placeholder'].'"':'')
            . (!empty($params['required'])?' required="1"':'')
            . ' label="'.$field_label.'"'
            . ' validate="'.$params['validate'].'"'
            ;
    foreach($params AS $k=>$v)
      if(preg_match('/^on\w+/',$k))
        $html[] = $k.'="'.$v.'" ';
    $html[] = ' >' . Form::htmlOut($value) . '</textarea>';
    if($params['span'])
      $html[] = '<span>'.$params['span'].'</span>';
    $html[] = '</div>';
    return implode(" ", $html)."\n";
  } // drawInputField

  // ************************************************************************************************************************************************************
  public static function drawSelectField( $field_label, $field_id, $selected=null, $params=Array() ){
    echo Form::getSelectField( $field_label, $field_id, $selected, $params );
  }
  public static function getSelectField( $field_label, $field_id, $selected=null, $params=Array() ){
    $params = (array)Form::getFieldParams($params, array('id' => Form::cleanId($field_id), 'name' => $field_id, 'label' => $field_label));
    $html = array();
    $options = isset($params['options']) ? $params['options'] : array();
    $html[] = '<div class="field field-select '.Form::cleanId($field_id).'">';
    $html[] = '<label for="'.Form::cleanId($field_id).'">'
            . ($params['tooltip']?Form::getToolTip($field_label,$params['tooltip']):$field_label)
            . ($params['required']?' *':'')
            . '</label>';
    $html[] = Form::getOptionBox( $options, ($params['name']?$params['name']:$field_id), $selected, $params );
    if($params['span'])
      $html[] = '<span>'.$params['span'].'</span>';
    $html[] = '</div>';
    return implode(" ", $html)."\n";
  } // drawSelectField

  // ************************************************************************************************************************************************************
  public static function drawCheckboxField( $field_label, $field_id, $selected=null, $params=Array() ){
    echo Form::getCheckboxField( $field_label, $field_id, $selected, $params );
  }
  public static function getCheckboxField( $field_label, $field_id, $selected=null, $params=Array() ){
    $params = (array)Form::getFieldParams($params, array('id' => Form::cleanId($field_id), 'name' => $field_id, 'label' => $field_label));
    $html = array();
    $options = isset($params['options']) ? $params['options'] : array();
    $html[] = '<div class="field field-checkbox '.Form::cleanId($field_id).'">';
    $html[] = '<label for="'.Form::cleanId($field_id).'">'
            . ($params['tooltip']?Form::getToolTip($field_label,$params['tooltip']):$field_label)
            . ($params['required']?' *':'')
            . '</label>';
    $html[] = Form::getCheckboxList( ($params['name']?$params['name']:$field_id), $options, $selected, $params );
    if($params['span'])
      $html[] = '<span>'.$params['span'].'</span>';
    $html[] = '</div>';
    return implode(" ", $html)."\n";
  } // drawCheckboxField

  // ************************************************************************************************************************************************************
  public static function drawRadioField( $field_label, $field_id, $selected=null, $params=Array() ){
    echo Form::getRadioField( $field_label, $field_id, $selected, $params );
  }
  public static function getRadioField( $field_label, $field_id, $selected=null, $params=Array() ){
    $params = (array)Form::getFieldParams($params, array('id' => Form::cleanId($field_id), 'name' => $field_id, 'label' => $field_label));
    $html = array();
    $options = isset($params['options']) ? $params['options'] : array();
    $html[] = '<div class="field field-radio '.Form::cleanId($field_id).'">';
    $html[] = '<label for="'.Form::cleanId($field_id).'">'
            . ($params['tooltip']?Form::getToolTip($field_label,$params['tooltip']):$field_label)
            . ($params['required']?' *':'')
            . '</label>';
    $html[] = Form::getRadioList( ($params['name']?$params['name']:$field_id), $options, $selected, $params );
    if($params['span'])
      $html[] = '<span>'.$params['span'].'</span>';
    $html[] = '</div>';
    return implode(" ", $html)."\n";
  } // drawRadioField

  // ************************************************************************************************************************************************************
  public static function drawCheckboxList( $field_label, $options, $selected=null, $params=Array() ){
    echo Form::getCheckboxList( $field_label, $options, $selected, $params );
  }
  public static function getCheckboxList( $field_id, $options, $selected=null, $params=Array() ){
    $params = (array)Form::getFieldParams($params, array('id' => Form::cleanId($field_id), 'name' => $field_id));
    $html = array(); $first = 0;
    if( !is_array($selected) ) $selected = array($selected);
    $html[] = '<dl class="options">';
    foreach( $options AS $option ){
      $list_field_id = Form::cleanId($field_id . ($first ? '_' . $first : ''));
      $html[] = '<dt>';
        $html[] = '<label for="'.$list_field_id.'">';
          $html[] = '<input
                      type="checkbox"
                      name="'.($field_id.(count($options)>1?'[]':'')).'"
                      id="'. $list_field_id .'"
                      value="'.$option->value.'" '.(in_array($option->value,$selected) ? 'checked' : '')
                  . ($first==0?' numallow="'.count($options).'" numrequire="1"':'')
                  . ($first==0 && $params['tooltip']?' title="'.Form::paramOut($field_label).'" tooltip="'.Form::paramOut($params['tooltip']).'"':'')
                  . $params['extra'] . ($params['disabled'] ? 'disabled="true"' : '')
                  . ($params['placeholder']?' placeholder="'.$params['placeholder'].'"':'')
                  // . ($params['required']?' required="1"':'')
                  . ($params['label']?' label="'.$params['label'].'"':'')
                  . ($params['validate']?' validate="'.$params['validate'].'"':'')
                  ;
          foreach($params AS $k=>$v)
            if(preg_match('/^on\w+/',$k))
              $html[] = $k.'="'.$v.'" ';
          $html[] = ' />';
          $html[] = '<span>'.$option->name.'</span>';
        $html[] = '</label>';
      $html[] = '</dt>';
      $first++;
    }
    $html[] = '</dl>';
    return implode(" ", $html) . "\n";
  } // getCheckboxList

  // ************************************************************************************************************************************************************
  public static function drawRadioList( $field_id, $options, $selected=null, $params=Array() ){
    echo Form::getRadioList( $field_label, $options, $selected, $params );
  }
  public static function getRadioList( $field_id, $options, $selected=null, $params=Array() ){
    $params = (array)Form::getFieldParams($params, array('id' => Form::cleanId($field_id), 'name' => $field_id));
    $html = array(); $first = 0;
    if( !is_array($selected) ) $selected = array($selected);
    $html[] = '<dl class="options">';
    foreach( $options AS $option ) {
      $list_field_id = Form::cleanId($field_id . ($first ? '_' . $first : ''));
      $html[] = '<dt>';
       $html[] = '<label for="'.$list_field_id.'">';
          $html[] = '<input type="radio" name="'.$field_id.'" id="'. $list_field_id.'" value="'.$option->value.'" '.(in_array((string)$option->value,$selected, true) ? 'checked' : '')
                  . ($first==0?' numallow="'.count($options).'" numrequire="1"':'')
                  . ($first==0 && $params['tooltip']?' title="'.Form::paramOut($field_label).'" tooltip="'.Form::paramOut($params['tooltip']).'"':'')
                  . (@$params['extra']?' '.$params['extra']:'')
                  . (@$params['disabled']?' disabled="true"':'')
                  . (@$params['placeholder']?' placeholder="'.$params['placeholder'].'"':'')
                  . (@$params['label']?' label="'.$params['label'].'"':'')
                  . (@$params['validate']?' validate="'.$params['validate'].'"':'')
                  ;
          foreach($params AS $k=>$v)
            if(preg_match('/^on\w+/',$k))
              $html[] = $k.'="'.$v.'" ';
          $html[] = ' />';
          $html[] = '<span>'.$option->name.'</span>';
        $html[] = '</label>';
      $html[] = '</dt>';
      $first++;
    }
    $html[] = '</dl>';
    return implode(" ", $html) . "\n";
  } // getRadioList

  // ************************************************************************************************************************************************************
  public static function drawOptionBox( &$options, $name, $selected=null, $params=array() ){
    echo Form::getOptionBox( $field_label, $options, $selected, $params );
  }
  public static function getOptionBox( &$options, $name, $selected=null, $params=array() ){
    $params = (array)Form::getFieldParams($params, array('id' => Form::cleanId($name), 'name' => $name));
    $out = '<select name="'.$name.($params['multiple']?'[]':'').'" id="'. Form::cleanId($name) .'"'
          .($params['multiple']?' multiple':'')
          .($params['style']?' style="'.$params['style'].'"':'')
          .($params['disabled']?' disabled="true"':'')
          .($params['class']?' class="'.$params['class'].'"':'')
          .($params['extra']?' '.$params['extra']:'')
          .($params['tooltip']?' title="'.Form::paramOut($field_label).'" tooltip="'.Form::paramOut($params['tooltip']).'"':'')
          .($params['placeholder']?' placeholder="'.$params['placeholder'].'"':'')
          .($params['required']?' required="1"':'')
          .($params['label']?' label="'.$params['label'].'"':'')
          .($params['validate']?' validate="'.$params['validate'].'"':'')
          .'>';
    foreach( $options AS $option )
      $out .= '<option value="'.$option->value.'"'
           .((is_array($selected) && in_array($option->value,$selected) || strlen($selected) && ($option->value==$selected) || is_null($selected) && $option->primary)?' selected':'')
           .'>'.$option->name.'</option>';
    $out .= '</select>';
    return $out;
  }

  // ************************************************************************************************************************************************************
  public static function showPageNav( $total, $list_start, $list_limit, $extra_hidden=null, $extra_fields=null, $form_name='adminForm' ){
    echo Form::getPageNav( $total, $list_start, $list_limit, $extra_hidden, $extra_fields, $form_name );
  }

  // ************************************************************************************************************************************************************
  public static function getPageNav( $total, $list_start, $list_limit, $extra_hidden=null, $extra_fields=null, $form_name = "adminForm", $task = null ){
    $task = ($task ?: Common::$app->request->request->get('task'));
    $parse_url = parse_url(Common::gimpLink('task='.$task));
    parse_str($parse_url['query'], $parse_query);
    $parse_query = array_merge($parse_query, array(
      'list_start' => $list_start,
      'task'       => $task
      ));
    $html = array();
    $html[] = '<form action="'.$parse_url['scheme'].'://'.$parse_url['host'].$parse_url['path'].'" name="'.$form_name.'" id="'.Form::cleanId($form_name).'" name="'.$form_name.'">';
    foreach ($parse_query AS $key => $val)
      $html[] = '<input type=hidden name="'. $key .'" value="'.$val.'" />';
    $html[] = $extra_hidden;
    $html[] = '<div class=pagenav>';
    $html[] = '<div class="pagenav-pages">';
    $html[] = '<input type=button class="btn btn-default" value="Back" onClick="document.'.$form_name.'.list_start.value='. ($list_start-$list_limit) .';document.'.$form_name.'.submit();" '. ($list_start>0?'':'disabled') .' />';
    if( $total > $list_limit ) {
      $btn_show = 10;
      $btn_pages = floor($total / $list_limit);
      $btn_start = $list_start > floor($btn_show/2) ? (($list_start > ($btn_pages - $btn_show)) ? $btn_pages - $btn_show : $list_start - floor($btn_show/2)) : 0;
      $btn_stop  = $btn_pages - $btn_start > $btn_show ? $btn_start + $btn_show : $btn_pages;
      if ($btn_start > 0)
        $html[] = '<a href="#" class="btn btn-sm btn-default" onClick="document.'.$form_name.'.list_start.value=0;document.'.$form_name.'.submit();">&lt;&lt;</a>';
      for( $i = $btn_start; $i <= $btn_stop; $i++ )
        $html[] = ($i==$list_start ? '<span class="btn btn-sm btn-default disabled">'.($i+1).'</span>' : '<a href="#" class="btn btn-sm btn-default" onClick="document.'.$form_name.'.list_start.value='.$i.';document.'.$form_name.'.submit();">'.($i+1).'</a>');
      if ($btn_stop < $btn_pages)
        $html[] = '<a href="#" class="btn btn-sm btn-default" onClick="document.'.$form_name.'.list_start.value='.$btn_pages.';document.'.$form_name.'.submit();">&gt;&gt;</a>';
    }
    $html[] = '<input type=button class="btn btn-default" value="Next" onClick="document.'.$form_name.'.list_start.value='.$list_start.';document.'.$form_name.'.submit();" '. ($list_start+$list_limit<$total?'':'disabled') .' />';
    $html[] = '( '. $total .' records Total )';
    $html[] = '</div>';
    $html[] = '<div class="pagenav-filter">';
    $html[] = 'View:';
    $html[] = '<select name="list_limit" onChange="document.'.$form_name.'.submit();">';
    $html[] = '<option value="10" '. ($list_limit==10 ? 'selected' : '') .'>10</option>';
    $html[] = '<option value="20" '. ($list_limit==20 ? 'selected' : '') .'>20</option>';
    $html[] = '<option value="50" '. ($list_limit==50 ? 'selected' : '') .'>50</option>';
    $html[] = '<option value="100" '. ($list_limit==100 ? 'selected' : '') .'>100</option>';
    $html[] = '<option value="200" '. ($list_limit==200 ? 'selected' : '') .'>200</option>';
    $html[] = '<option value="500" '. ($list_limit==500 ? 'selected' : '') .'>500</option>';
    $html[] = '<option value="999" '. ($list_limit==999 ? 'selected' : '') .'>999</option>';
    $html[] = '</select>';
    $html[] = $extra_fields;
    $html[] = '</div>';
    $html[] = '</div>';
    $html[] = '</form>';
    return implode(" ", $html)."\n";
  }

}
