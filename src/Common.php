<?php

namespace WebuddhaInc\FormGimp;

class Common {

  public static $app;
  public static $coreFormFields = array(
                                  'id',
                                  'fullname',
                                  'email',
                                  'phone',
                                  'subject',
                                  'description',
                                  'referrer',
                                  'created',
                                  'reviewed',
                                  'archived',
                                  'data',
                                  'history'
                                  );

  // ************************************************************************************************************************************************************
  public static function gimpLink( $params = null ){
    return self::$app->route($params);
  }

  // ************************************************************************************************************************************************************
  public static function getIndexArray( $table ){
    global $gimpDB;
    if( !strlen($table) ) return null;
    $gimpDB->query("
      SELECT *
      FROM `#__wbfg_index`
      WHERE `table` = '". $gimpDB->escape($table) ."'
      ");
    $index = $gimpDB->getRows();
    if ($index) {
      $index = (array)reset($index);
      foreach (array('field_types', 'email_alerts', 'params') AS $key)
        $index[$key] = @$index[$key] ? unserialize($index[$key]) : null;
    }
    return (array)$index;
  }

  // ************************************************************************************************************************************************************
  public static function loadSetupGlobals(){
    global $gimpDB;
    $gimpDB->query("SELECT * FROM #__wbfg_setup ORDER BY name;");
    $setups = $gimpDB->getRows();
    foreach( $setups AS $setup ) {
      $setup = (array)$setup;
      $GLOBALS['fg_'.$setup['name']] = $setup['value'];
    }
  }

  // ************************************************************************************************************************************************************
  public static function saveSetupKey( $key, $value ){
    global $gimpDB;
    $gimpDB->query("
      INSERT INTO `#__wbfg_setup`
      (`name`, `value`)
      VALUES
      ('".$gimpDB->escape($key)."', '".$gimpDB->escape($value)."')
      ON DUPLICATE KEY
        UPDATE `value` = '".$gimpDB->escape($value)."'
      ");
  }

  // ************************************************************************************************************************************************************
  public static function loadTableCache( $session_id, $table ){
    global $gimpDB;
    if( $session_id ){
      $gimpDB->query("
        SELECT data
        FROM `#__wbfg_cache`
        WHERE `session_id` = '". $gimpDB->escape($session_id) ."'
          AND `table` = '". $gimpDB->escape($table) ."'
        ORDER BY `created` DESC
        LIMIT 1
        ");
      $rows = $gimpDB->getRows();
      if( count($rows) )
        return unserialize($rows[0]->data);
      else
        return Array();
    } else
      return Array();
  }

  // ************************************************************************************************************************************************************
  public static function saveTableCache( $session_id, $table ){
    global $gimpDB; if( !$session_id )return;
    Common::deleteTableCache( $session_id, $table );
    $gimpDB->query("
      INSERT INTO `#__wbfg_cache` (`session_id`,`table`,`created`,`data`)
      VALUES ('". $gimpDB->escape($session_id) ."','". $gimpDB->escape($table) ."',NOW(),'". $gimpDB->escape(serialize(Common::$app->request->request->get('field'))) ."')
      ");
  }

  // ************************************************************************************************************************************************************
  public static function deleteTableCache( $session_id, $table ){
    global $gimpDB; if( !$session_id )return;
    $gimpDB->query("
      DELETE FROM #__wbfg_cache
      WHERE `session_id` = '". $gimpDB->escape($session_id) ."'
        AND `table` = '". $gimpDB->escape($table) ."'
      ");
  }

  // ************************************************************************************************************************************************************
  public static function loadSubmission($table, $table_id){
    global $gimpDB;
    $table = preg_replace('/[^a-z\_]/','',$table);
    $table_id = (int)$table_id;
    $gimpDB->query("
      SELECT *
      FROM `#__wbfg_form_". $gimpDB->escape($table) ."`
      WHERE `id` = '". $gimpDB->escape($table_id) ."'
      ");
    $row = $gimpDB->getRows();
    if ($row) {
      $row = reset($row);
      $row->data = $row->data ? unserialize($row->data) : null;
      $row->history = $row->history ? unserialize($row->history) : null;
      $gimpDB->query("
        SELECT *
        FROM `#__wbfg_attachments`
        WHERE `table` = '". $gimpDB->escape($table) ."'
        AND `table_id` = '". $gimpDB->escape($table_id) ."'
        ORDER BY `field`
          , `filename`
          , `size` DESC
        ");
      $row->_attachments = (array)$gimpDB->getRows();
    }
    return $row;
  }

  // ************************************************************************************************************************************************************
  public static function archiveSubmissions(){
    global $gimpDB, $fg_archive_timeout;
    $gimpDB->query("SELECT * FROM #__wbfg_index");
    $timeout = (int)$fg_archive_timeout ?: 1;
    $forms = $gimpDB->getRows();
    foreach( $forms AS $form ){
      $table = '#__wbfg_form_'.$form['table'];
      $gimpDB->query("
        UPDATE `". $gimpDB->escape($table) ."`
        SET `archived` = 1
        WHERE `created` < '".sql_date_shift(date('Y-m-d H:i:s'), "-". (int)$timeout ." days")."'
        ");
    }
  }

  // ************************************************************************************************************************************************************
  public static function sql_date_shift($date, $shift) {
    return date("Y-m-d H:i:s" , strtotime($shift, strtotime($date)));
  }

  // ************************************************************************************************************************************************************
  public static function htmlOut( $value, $def = '' ){
    return Form::htmlOut($value, $def);
  }

  // ************************************************************************************************************************************************************
  public static function formatFileSize( $fileSize ){
    if( !$fileSize || ($fileSize < 0) )
      return 0;
    else
      if( $fileSize < 1024 )
        return $fileSize.' bytes';
      elseif ($fileSize < pow(1024,2))
        return round($fileSize/1024,2).' KB';
      elseif ($fileSize < pow(1024,3))
        return round($fileSize/pow(1024,2),2).' MB';
      elseif ($fileSize < pow(1024,4))
        return round($fileSize/pow(1024,3),2).' GB';
      else
        return round($fileSize/pow(1024,4),2).' TB';
  }

  // ************************************************************************************************************************************************************
  public static function send_mail($to_address, $from_address, $email_subject, $email_body){

    // Build HTML Header
    $email_headers = '';
    $email_headers .= 'From: ' . $from_address . "\n";
    $email_headers .= 'Reply-To: ' . $from_address . "\n";
    $email_headers .= 'X-Mailer: PHP/' . phpversion() . "\n";
    $email_headers .= 'MIME-Version: 1.0' . "\n";
    $email_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\n";
    $email_headers .= 'Content-Transfer-Encoding: 8bit' . "\n";

    // Send Email
    if( eval('return @mail( $to_address, $email_subject, $email_body, $email_headers );') )
      return 1;

  }

  /**
   * [xml_getFields description]
   * @param  [type] $xmlFile [description]
   * @param  [type] $xpath   [description]
   * @return [type]          [description]
   */
  public static function xml_getFields($xmlFile, $xpath='/form/fieldset[@name=\'params\']/fields/field'){
    $xmlFields = array();
    try {
      $xml = new \SimpleXMLElement(is_file($xmlFile) ? file_get_contents($xmlFile) : $xmlFile);
      $xmlFields = $xml->xpath($xpath);
    } catch (Exception $e) {
    }
   return $xmlFields;
  }

  /**
   * [xml_getFieldDefaults description]
   * @param  [type] $xmlFile [description]
   * @param  [type] $xpath   [description]
   * @return [type]          [description]
   */
  public static function xml_getFieldDefaults($xmlFile, $xpath='/form/fieldset[@name=\'params\']/fields/field'){
    $xmlFieldDefaults = array();
    try {
      $xmlFields = Common::xml_getFields($xmlFile, $xpath);
      foreach ($xmlFields AS $xmlField) {
        $xmlFieldDefaults[(string)@$xmlField['name']] = (string)@$xmlField['default'];
      }
    } catch (Exception $e) {
    }
    return $xmlFieldDefaults;
  }

  /**
   * [aes_encrypt description]
   * @param  [type] $str [description]
   * @return [type]      [description]
   */
  public static function aes_encrypt( $str ){
    global $gimpDB;
    $crypt_key = strrev(sha1(self::$app->config->get('crypt_key')));
    return base64_encode(hex2bin($gimpDB->query("SELECT HEX(AES_ENCRYPT('". $gimpDB->escape($str) ."', '". $gimpDB->escape($crypt_key) ."')) AS `val`")->getValue()));
  }

  /**
   * [aes_decrypt description]
   * @param  [type] $crypt_str [description]
   * @return [type]            [description]
   */
  public static function aes_decrypt( $crypt_str ){
    global $gimpDB;
    $crypt_key = strrev(sha1(self::$app->config->get('crypt_key')));
    return $gimpDB->query("SELECT AES_DECRYPT(UNHEX('". bin2hex(base64_decode($crypt_str)) ."'), '". $gimpDB->escape($crypt_key) ."') AS `val`")->getValue();
  }

}