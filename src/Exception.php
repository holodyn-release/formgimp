<?php

namespace WebuddhaInc\FormGimp;

class Exception extends \Exception {

  public function __construct($message = null, $code = 0, Exception $previous = null){
    parent::__construct($message, $code, $previous);
    switch ($code) {
      case 500:
        echo '<div class="message error fatal">' . $message . '</div>';
        break;
      case 400:
        echo '<div class="message error">' . $message . '</div>';
        break;
      case 300:
        echo '<div class="message warning">' . $message . '</div>';
        break;
      default:
      case 200:
        echo '<div class="message notice">' . $message . '</div>';
        break;
    }
  }

}