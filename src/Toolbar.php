<?php namespace WebuddhaInc\FormGimp;

class Toolbar {

  static function render($task){
    switch ($task) {
      case 'forms':
        self::_FORMS();
        break;
      case 'forms.edit':
        self::_FORMS_EDIT();
        break;
      case 'forms.submissions':
        self::_SUBMISSIONS();
        break;
      case 'forms.submissions.view':
        self::_SUBMISSIONS_VIEW();
        break;
      case 'setup':
        self::_SETUP();
        break;
    }
  }

  static function _RENDER($title, $buttons){
    $html = array();
    $html[] = '<div class="formgimp toolbar">';
    $html[] = '<h3>'. $title .'</h3>';
    $html[] = '<ul>';
    foreach ($buttons as $button) {
      if ($button)
        $html[] = '<li>'. $button .'</li>';
    }
    $html[] = '</ul>';
    $html[] = '</div>';
    echo implode('', $html);
  }

  static function _FORMS() {
    self::_RENDER('Manage Forms',array(
      '<button class="btn btn-primary" onclick="submitForm(\'forms.edit\');">New</button>'
      ));
  }

  static function _FORMS_EDIT() {
    self::_RENDER('Edit Form',array(
      (!@$_REQUEST['table'] ? '' : '<button class="btn btn-primary" onclick="submitForm(\'forms.edit.save\');">Save</button>'),
      '<button class="btn btn-success" onclick="submitForm(\'forms.edit.apply\');">Apply</button>',
      '<button class="btn" onclick="submitForm(\'forms.edit.cancel\');">Cancel</button>',
      (!@$_REQUEST['table'] ? '' : '<button class="btn btn-danger" onclick="submitForm(\'forms.edit.delete\');">Delete</button>')
      ));
  }

  static function _SUBMISSIONS() {
    self::_RENDER('Manage Submissions',array(
      '<button class="btn" onclick="submitForm(\'forms\');"><< Back</button>',
      '<button class="btn btn-success" onclick="submitForm(\'forms.submissions.archive\');">Archive</button>',
      '<button class="btn btn-warning" onclick="submitForm(\'forms.submissions.unarchive\');">Unarchive</button>',
      '<button class="btn btn-default" onclick="submitForm(\'forms.submissions.export\');">Export</button>',
      '<button class="btn btn-danger" onclick="submitForm(\'forms.submissions.delete\');">Delete</button>'
      ));
  }

  static function _SUBMISSIONS_VIEW() {
    $submission = Common::loadSubmission($_REQUEST['table'], $_REQUEST['id']);
    self::_RENDER('View Submission',array(
      '<button class="btn btn-default" onclick="submitForm(\'forms.submissions\');"><< Back</button>',
      (
        $submission->archived
        ? '<button class="btn btn-warning" onclick="submitForm(\'forms.submissions.unarchive\');">Unarchive</button>'
        : '<button class="btn btn-success" onclick="submitForm(\'forms.submissions.archive\');">Archive</button>'
        ),
      '<button class="btn btn-danger" onclick="submitForm(\'forms.submissions.delete\');">Delete</button>'
      ));
  }

  static function _SETUP() {
    self::_RENDER('Setup',array(
      '<button class="btn btn-primary" onclick="submitForm(\'setup.save\');">Save</button>'
      ));
  }

}
