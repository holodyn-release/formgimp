<?php namespace WebuddhaInc\FormGimp;

if (!$index || !$submission) {
  throw new \Exception('Index and Submission objects required');
}

?>
<html>
  <head>
    <title><?php echo $content->title ?></title>
    <style>
      h1, h2, div, p, th, td, input {
        font-family: tahoma;
        font-size: 12px;
      }
      h1 {
        font-size: 14px;
      }
      h1.alert {
        color: #FFFF00;
        background: #FF0000;
        padding: 3px;
        text-align: center;
      }
      table.header tr td label {
        font-size: 11px;
        font-weight: bold;
        color: #FFFFFF;
      }
    </style>
  </head>
  <body style="background:#efefef;margin:0px;">
    <h1><?php echo $content->title ?></h1>
    <?php echo $content->text ?>
  </body>
</html>
