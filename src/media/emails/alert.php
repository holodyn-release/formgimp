<?php namespace WebuddhaInc\FormGimp;

if (!$index || !$submission) {
  throw new \Exception('Index and Submission objects required');
}

?>
<html>
  <head>
    <title>FormGimp by Webuddha.com</title>
    <style>
      h1, h2, div, p, th, td, input {
        font-family: tahoma;
        font-size: 12px;
      }
      h1 {
        font-size: 14px;
      }
      h1.alert {
        color: #FFFF00;
        background: #FF0000;
        padding: 3px;
        text-align: center;
      }
      table.header tr td label {
        font-weight: bold;
        color: #FFFFFF;
      }
      table.attachments tr th {
        background: none;
        border: none;
        text-align: left;
        vertical-align: top;
      }
      table.attachments tr td {
        border: 0px;
        text-align: left;
        vertical-align: top;
      }
      table.attachments tr td a,
      table.attachments tr td a:visited {
        text-decoration: none;
      }
    </style>
  </head>
  <body style="background:#efefef;margin:0px;">
    <table border=0 cellpadding=0 cellspacing=0 width=100% height=100%>
      <tr height=10><td>
        <table class="header" border=0 cellpadding=0 cellspacing=0 width=100%>
          <tr><td><?= $this->app->mediaInclude('img', 'images/header.jpg') ?></td></tr>
        </table>
      </td></tr>
      <tr><td valign=top>
        <table border=0 cellpadding=10 cellspacing=0 width=100%>
          <tr><td>
            <h1>A new submission has been received from <?= Form::htmlOut($submission->fullname) ?> (<?= Form::htmlOut($submission->email) ?>)</h1>
            <hr/>
            <table border=0 cellpadding=2 cellspacing=0 width=100% class=attachments>
              <tr>
                <th NOWRAP>Full Name:</td>
                <td><?= Form::htmlOut($submission->fullname) ?></td>
              </tr>
              <tr>
                <th NOWRAP>Email:</td>
                <td><a href="mailto:<?= Form::paramOut($submission->fullname) ?> (<?= Form::paramOut($submission->email) ?>)" title="Send Email"><?= Form::htmlOut($submission->email) ?></a></td>
              </tr>
              <tr>
                <th NOWRAP>Phone:</td>
                <td><?= Form::htmlOut($submission->phone) ?></td>
              </tr>
              <tr>
                <th NOWRAP>Referrer:</td>
                <td><?= Form::htmlOut($submission->referrer) ?></td>
              </tr>
              <tr>
                <th NOWRAP>Subject:</td>
                <td><?= Form::htmlOut($submission->subject) ?></td>
              </tr>
              <tr>
                <th NOWRAP>Description:</td>
                <td><?= Form::htmlOut($submission->description) ?></td>
              </tr>
              <?php
                $formFields = (array)@$index['field_types'];
                foreach ($formFields AS $field) {
                  if( @$field['name'] && !in_array(@$field['name'],Common::$coreFormFields) ){
                    ?>
                    <tr>
                      <th><?= Form::htmlOut($field['label']) ?>:</td>
                      <td><?= Form::htmlOut(is_array($submission->data[$field['name']]) ? implode(', ', $submission->data[$field['name']]) : $submission->data[$field['name']]) ?></td>
                    </tr>
                    <?php
                  }
                }

              ?>
              <tr><td colspan=2><hr/></td></tr>
              <tr>
                <th NOWRAP>Created:</td>
                <td><?= Form::htmlOut($submission->created) ?></td>
              </tr>
              <tr>
                <th NOWRAP>Reviewed:</td>
                <td><?= Form::htmlOut($submission->reviewed) ?></td>
              </tr>
              <tr>
                <th NOWRAP>Archived:</td>
                <td><?= $submission->archived ? 'YES' : 'NO' ?></td>
              </tr>
            </table>
            <?php if( $submission->_attachments ){ ?>
            <hr/>
            <table border=0 cellpadding=5 cellspacing=0 width=100% class=attachments>
              <?php
                $rowCount = 1;
                foreach ($submission->_attachments AS $attachment) {
                  $attachment = (array)$attachment;
                  $fileName = $attachment['filename'];
                  if( strlen($fileName) > 48 )
                    $fileName = preg_replace('/^(.{48}).*$/','$1',$fileName).'...';
                  echo '<tr>';
                  echo '<th>'.$rowCount++.') </th>';
                  echo '<td>'.$attachment['field'].'</td>';
                  echo '<td><a href="'. $this->app->route('task=download&format=raw&eid='.Common::aes_encrypt($attachment['id'])) .'" title="'.$attachment['filename'].'">'.$fileName.'</a></td>';
                  echo '<td>'. Common::formatFileSize($attachment['size']) .'</td>';
                  echo '</tr>';
                }
              ?>
            </table>
            <?php } ?>
            <hr/>
            <a href="<?= $this->app->route('task=submission&xid='.Common::aes_encrypt($index['table'].','.$submission->id)) ?>">Click Here</a> to view the submission,
            or <a href="<?= $this->app->route('task=forms.submissions.view&table=' . $index['table'] .'&id='. $submission->id) ?>">Click Here</a> to login and manage this submission.
          </td></tr>
        </table>
      </td></tr>
    </table>
  </body>
</html>
