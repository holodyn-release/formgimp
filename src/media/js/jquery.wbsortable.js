// ========================================================================
// wbSortable Table and List Element Sorting
// Licensed under the GNU/GPL Open Source License
// (c) 2008 Webuddha.com, The Holodyn Corporation
// Version: 1.2.0
// Release: 020509
// ========================================================================

function wbSortable(obj,post){
  // ===================== config
  this.option = {
    mode:'list',
    post:post,
    ghost:true,
    ghost_target:true,
    ghost_opacity:{
      list: 70,
      table: 40
    },
    csson:true
  };
  this.css = {
    drag:'wbs_drag',
    child:'wbs_child',
    target:'wbs_target',
    invalid:'wbs_invalid',
    row0:'wbs_row0',
    row1:'wbs_row1'
  };
  this.isOn=false;
  this.act=null;
  this.actBdy=null;
  this.mto=null;
  this.tbRows=[];
  this.tgRows=[];
  this.tgFirst=[];
  this.tgList=[];
  this.tgFirstId=0;
  this.mso={x:0,y:0};
  this.instVar=null;
  this.instObj=null;
  // ===================================================================
  // User Functions
  this.reload   = function(){ this.instObj=document.getElementById(this.instObj.id);this._init(); };
  this.enable   = function(mo){ if(typeof mo=='object')mo.drag=true;  else this.isOn=true; };
  this.disable  = function(mo){ if(typeof mo=='object')mo.drag=false; else this.isOn=false; };
  // ===================================================================
  // Initialization
  this._init = function() {
    // reset
    this.act=null;this.actBdy=null;this.mto=null;
    this.tbRows=[];this.tgRows=[];this.tgFirst=[];this.tgList=[];
    this.tgFirstId=0; this.mso={x:0,y:0}; this.isOn=true;
    // confirm
    if(typeof this.instVar!='string')return alert('Bad InstVar');
    if(typeof this.instObj!='object')return alert('Bad InstObj');
    if(typeof this.option.mode!='string')return alert('Bad Mode');
    if(typeof this.option.post!='string')return alert('Bad Post');
    // initialize
    if(this.option.mode=='table'){
      for(var bC=0;bC<this.instObj.tBodies.length;bC++){
        this.actBdy=this.tbRows[bC]=this.instObj.tBodies[bC].rows;
        for(var nC=0;nC<this.actBdy.length;nC++)
          this._set(this.actBdy[nC],bC);
      };
    } else if(this.option.mode=='list'){
      var bC=0;this.tbRows = [];
      for(var nC=0;nC<this.instObj.childNodes.length;nC++){
        if(this.instObj.childNodes[nC].nodeName=='UL'){
          if(this.tbRows.length)bC++;
          this.tbRows[bC]=this.instObj.childNodes[nC];
        };
      };
      if(!this.tbRows.length)this.tbRows = [this.instObj];
      for(var bC=0;bC<this.tbRows.length;bC++){
        this.actBody = this.tbRows[bC]; this.tbRows[bC] = [];
        for(var nC=0;nC<this.actBody.childNodes.length;nC++){
          if(this.actBody.childNodes[nC].nodeName=='LI'){
            this.tbRows[bC].push(this.actBody.childNodes[nC]);
            this._set(this.actBody.childNodes[nC],bC);
          };
        };
      };
    } else return alert('Invalid Mode');
    for(var a=0;a<this.tbRows.length;a++){
      for(var b=0;b<this.tbRows[a].length;b++){
        if(this.tbRows[a][b].id){
          this.tbRows[a][b].gNodes = 0;
          for(var c=0;c<this.tbRows[a].length;c++){
            if(this._icr(this.tbRows[a][c].tiId,this.tbRows[a][b].tiId))
              this.tbRows[a][b].gNodes++;
          };
        };
      };
    };
    if(this.option.ghost){
      this.ghst = document.getElementById('wbsortable_ghost_'+this.instVar);
      if(this.ghst == null){
        if(this.option.mode=='list'){
          this.ghst=document.createElement('LI');
          this.ghst.style['-moz-opacity'] = '0.'+this.option.ghost_opacity.list;
          this.ghst.style['opacity'] = '.'+this.option.ghost_opacity.list;
          this.ghst.style['filter'] = 'alpha(opacity='+this.option.ghost_opacity.list+')';
        } else {
          this.ghst=document.createElement('DIV');
          this.ghst.style['-moz-opacity'] = '0.1';
          this.ghst.style['opacity'] = '.10';
          this.ghst.style['filter'] = 'alpha(opacity='+this.option.ghost_opacity.table+')';
        }
        this.ghst.setAttribute('id','wbsortable_ghost_'+this.instVar);
        this.ghst.className = 'wbsortable_ghost';
        this.ghst.style['display'] = 'none';
        this.ghst.style['position'] = 'absolute';
        this.ghst.style['backgroundColor'] = '#00FF00';
        this.ghst.style['padding'] = '0 5px 5px 0';
        this.ghst.style['zIndex'] = '420';
        this.docb=document.getElementsByTagName('body').item(0);
        this.docb.insertBefore(this.ghst,this.docb.firstChild);
      };
    };
    return this;
  };
  // ===================================================================
  // Action: Activate Dragging
  this._set = function(item,tbId) {
    if(!item||!item.id)return;
    item.tbId=tbId;
    if(item.id.match(/^\w+\_\w+$/)){
      item.tgId=item.id.replace(/\_.*$/,'');
      item.tiId=item.id.replace(/^\w+\_/,'');
    } else {
      item.tgId=0;
      item.tiId=item.id;
    }
    if(!this.tgFirst[item.tgId])
      this.tgFirst[item.tgId]=item.tiId;
    item.po=this;
    item.mso=this._gop(item);
    item.style.cursor='move';
    item.onmousedown=this._imd;
    item.onmouseover=this._imo;
    item.onselectstart = function(){ return false; }; // ie
    if(typeof this.tgRows[item.tgId]=='undefined')
      this.tgRows[item.tgId] = [];
    this.tgRows[item.tgId].push(item);
  };
  // ===================================================================
  // Action: Move Row Down
  this._mrdn = function(dO,cO){
    if(!dO||!cO)return;
    var gNodes = dO.gNodes;
    for(var c=0;c<this.actBdy.length;c++){
      if(this.actBdy[c].id == cO.id){
        cO = this.actBdy[c+cO.gNodes];
        for(var i=0;i<this.actBdy.length;i++){
          if(this.actBdy[i].id == dO.id){
            for(var m=0;m<=gNodes;m++)
              dO.parentNode.insertBefore(this.actBdy[(i+gNodes)-m],cO.nextSibling);
            dO.parentNode.insertBefore(dO,cO.nextSibling);
            return false;
          };
        };
      };
    };
  };
  // ===================================================================
  // Action: Move Row Up
  this._mrup = function(dO,cO){
    if(!dO||!cO)return;
    var gNodes = dO.gNodes;
    for(var i=0;i<this.actBdy.length;i++){
      if(this.actBdy[i].id == dO.id){
        dO.parentNode.insertBefore(dO,cO);
        for(var m=0;m<gNodes;m++)
          this.actBdy[i].parentNode.insertBefore(this.actBdy[++i],cO);
        return false;
      };
    };
  };
  // ===================================================================
  // Action: Drop Active
  this._ond = function(row) {
    this.tgList=[];
    if(this.actBdy.length){
      if(this.option.mode=='table')nP=this.actBdy;
      else nP=this.actBdy[0].parentNode.childNodes;
      for(var i=0;i<nP.length;i++){
        if(nP[i].tgId==row.tgId)
          this.tgList.push(nP[i].tiId);
        if(this.option.mode=='list')
          this.actBdy[i] = nP[i];
      };
      this.tgFirstId=this.tgFirst[row.tgId];
      if(this.tgList.length)
        this.tgFirst[row.tgId]=this.tgList[0];
      if(this.option.post)
        try{eval(this.option.post+'(this.tgList,this.tgFirstId,row.tgId);');}
          catch(e) { /* Not Found... alert(e); */ this.option.post=''; };
    };
  };
  // ===================================================================
  // Action: Set Active Class
  this._sac = function(mo,on){
    if(!this.actBdy||!this.option.csson)return;
    if(on)var tG = mo.tgId;var gn = 0;
    for(var bgc=0;bgc<this.actBdy.length;bgc++){
      if(this.actBdy[bgc].disabled)continue;
      if(this.actBdy[bgc].id == mo.id){
        this.actBdy[bgc].className = (on?this.css.drag:(bgc%2?this.css.row1:this.css.row0));
        gn=mo.gNodes;
      } else if(gn){
        this.actBdy[bgc].className = (on?this.css.child:(bgc%2?this.css.row1:this.css.row0));
        gn--;
      } else if(on) {
        if(this.actBdy[bgc].tgId == tG){
          this.actBdy[bgc].mso=this._gop(this.actBdy[bgc]);
          this.actBdy[bgc].className = this.css.target;
        } else {
          this.actBdy[bgc].className = this.css.invalid;
        };
      } else {
        this.actBdy[bgc].className = (bgc%2?this.css.row1:this.css.row0);
      };
    };
  };
  // ===================================================================
  // Check: is Child Node
  this._icr = function(Id,group){
    if(!this.tgRows[group])return false;
    if(Id==group)return false;
    var grps=this.tgRows[group];
    for(var gC=0;gC<grps.length;gC++){
      if(Id==grps[gC].tiId) return true;
      if(this._icr(Id,grps[gC].tiId)) return true;
    };
    return false;
  };
  // ===================================================================
  // Check: Object Position/Dimensions
  this._gop = function(mo){
    var xP=0,yP=0,cW=(mo.clientWidth||mo.offsetWidth||mo.innerWidth),
      cH=(mo.clientHeight||mo.offsetHeight||mo.innerHeight);
    if(mo.offsetHeight==0)mo=mo.firstChild;if(!mo)return{x:xP,y:yP,w:cW,h:cH};
    while(mo.offsetParent){xP+=mo.offsetLeft;yP+=mo.offsetTop;mo=mo.offsetParent;};
    xP+=mo.offsetLeft;yP+=mo.offsetTop;return{x:xP,y:yP,w:cW,h:cH};
  };
  // ===================================================================
  // Event: Get Mouse Position
  this._gmp = function(ev){
    ev=(ev||window.event);var dE=document.documentElement;
    if(ev.pageX||ev.pageY)return{x:ev.pageX,y:ev.pageY};
    else if(ev.page)return{x:ev.page.x,y:ev.page.y};
    else return{x:ev.clientX+dE.scrollLeft,y:ev.clientY+dE.scrollTop};
  };
  // ===================================================================
  // Event: Item MouseDown
  this._imd = function(e){
    if(this.po.act)return;
    if(!this.po.isOn)return;
    if(this.po._egt(e).drag===false)return;
    var tag=this.po._egt(e).nodeName;
    var pTag=this.po._egt(e).parentNode.nodeName;
    if(/^A$|INPUT|SELECT|TEXTAREA|BUTTON/.test(tag) || /^A$|INPUT|SELECT|TEXTAREA|BUTTON/.test(pTag))return true;
    if(this.disabled)return true;
    this.po.act=this;
    this.po.actBdy=this.po.tbRows[this.tbId];
    this.po._sac(this,1);
    if(this.po.option.ghost)this.po._ghost(this,e);
    jQuery(document).on('mouseup', eval('(function(e){'+this.po.instVar+'._emu(e);})'));
    return false;
  };
  // ===================================================================
  // Event: Ghost Item Track
  this._gtrk = function(ev){
    if(this.ghst.ok){
      var mP=this._gmp(ev);
      if(this.option.mode=='list')
        this.ghst.style.left=mP.x-(parseInt(this.ghst.style.width,10)/2)+'px';
        else this.ghst.style.left=this.ghst.mso.x+'px';
      this.ghst.style.top=mP.y-(parseInt(this.ghst.style.height,10)/2)+'px';
      for(var oI=0;oI<this.tgRows[this.ghst.obj.tgId].length;oI++){
        var oO=this.tgRows[this.ghst.obj.tgId][oI];
        if(mP.x>oO.mso.x&&mP.x<oO.mso.x+oO.mso.w&&mP.y>oO.mso.y&&mP.y<oO.mso.y+oO.mso.h)
          oO.onmouseover(ev);
      };
    } return false;
  };
  // ===================================================================
  // Action: Ghost Item Toggle
  this._ghost = function(mo,ev){
    if(mo){
      this.ghst.obj=mo;
      this.ghst.mso=mo.mso;
      if(this.option.mode=='list'){
        this.ghst.className=mo.className;
        this.ghst.innerHTML=mo.innerHTML;
        mo.parentNode.insertBefore(this.ghst,mo);
        if(this.option.ghost_target)
          mo.style.visibility='hidden';
      }
      this.ghst.style.width=mo.mso.w+'px';
      this.ghst.style.height=mo.mso.h+'px';
      this.ghst.style.display='block';
      this.ghst.ok=1;
      this._gtrk(ev);
      jQuery(document).on('mousemove', eval('(function(e){'+this.instVar+'._gtrk(e);})'));
    }else{
      if(this.option.mode=='list')
        if(this.option.ghost_target)
          this.ghst.obj.style.visibility='visible';
      jQuery(document).unbind('mousemove', eval('(function(e){'+this.instVar+'._gtrk(e);})'));
      this.ghst.ok=0;
      this.ghst.style.display='none';
      if(this.option.mode=='list')
        this.docb.insertBefore(this.ghst,this.docb.firstChild);
    };
  };
  // ===================================================================
  // Event: Item MouseOver
  this._imo = function(e){
    if(this.po&&this.po.act&&!this.po.proc&&this.po.act!=this
      &&this.po.act.tbId==this.tbId&&this.po.act.tgId==this.tgId){
      if(this.po.mto)clearTimeout(this.po.mto);
      this.po.targ=this;this.po.mto=setTimeout(this.po.instVar+'._imop();',75);
    } return false;
  };
  // ===================================================================
  // Event: Item MouseOver Processor
  this._imop = function(){
    if(!this.act||!this.targ)return false;
    this.proc=1;
    var dir=(this.act.mso.y<=this.targ.mso.y);
    if(this.option.mode=='list'&&this.act.mso.y==this.targ.mso.y)
      dir=(this.act.mso.x<=this.targ.mso.x);
    if(dir)this._mrdn(this.act,this.targ);
      else this._mrup(this.act,this.targ);
    for(var gR=0;gR<this.tgRows[this.act.tgId].length;gR++)
      this.tgRows[this.act.tgId][gR].mso=this._gop(this.tgRows[this.act.tgId][gR]);
    this.proc=0;
    return false;
  };
  // ===================================================================
  // Event: onMouseUp
  this._emu = function(e){
    if(this.act){
      if(this.option.ghost)this._ghost(null,e);
      jQuery(document).unbind('mouseup', eval('(function(e){'+this.instVar+'._emu(e);})'));
      this._ond(this.act);
      this._sac(this.act);
      this.act = null;
    };
  };
  // ===================================================================
  // Event: Get Target
  this._egt = function(e) {
    if(window.event)return window.event.srcElement; // IE
      else return e.target; // FF
  };
  // ===================================================================
  // Event: Get Milliseconds
  this._gms = function(){ var d=new Date();return d.getTime(); };
  // ===================================================================
  // Event: Setup Instance
  if(typeof document!='object')return;
  if(typeof obj=='string')obj=document.getElementById(obj);
  if(typeof obj!='object')return;
  if(obj.nodeName=='UL')this.option.mode='list';
    else if(obj.nodeName=='TABLE')this.option.mode='table';else return;
  if(typeof wbSortableLog!='object')eval('wbSortableLog=[];');
  if(typeof obj.id!='string')obj.id=('wbs'+Math.random()).replace(/[^\d\w]+/,'');
  if(wbSortableLog.length)for(var k in wbSortableLog)if(k==obj.id)return wbSortableLog[k].reload();
  this.instVar=('wbsi'+Math.random()).replace(/[^\d\w]+/,'');this.instObj=obj;
  wbSortableLog[obj.id]=this;eval(this.instVar+'=this._init();');
};
