/*
************************************************************************

wbForm
Licensed under the GNU/GPL Open Source License
(c) 2008 Webuddha.com, The Holodyn Corporation

************************************************************************
*/
/* ------------------------------------------------- */
.wbform * {
  box-sizing: border-box;
  vertical-align: top;
}
.wbform .field_alert {
  line-height: 28px;
  color: red;
}
.wbform .field {
  margin: 4px;
}
.wbform .field label {
  display: inline-block;
  padding-right: 5px;
  line-height: 28px;
}
.wbform .field input[type=text],
.wbform .field input[type=password],
.wbform .field input[type=color],
.wbform .field input[type=date],
.wbform .field input[type=datetime],
.wbform .field input[type=datetime-local],
.wbform .field input[type=email],
.wbform .field input[type=month],
.wbform .field input[type=number],
.wbform .field input[type=range],
.wbform .field input[type=search],
.wbform .field input[type=tel],
.wbform .field input[type=time],
.wbform .field input[type=url],
.wbform .field input[type=week],
.wbform .field select,
.wbform .field textarea {
  min-height: 26px;
  line-height: 18px;
  padding: 4px;
  margin: 0;
  border: 1px solid rgba(0, 0, 0, 0.5);
}
.wbform .field.active input[type=text],
.wbform .field.active input[type=password],
.wbform .field.active input[type=color],
.wbform .field.active input[type=date],
.wbform .field.active input[type=datetime],
.wbform .field.active input[type=datetime-local],
.wbform .field.active input[type=email],
.wbform .field.active input[type=month],
.wbform .field.active input[type=number],
.wbform .field.active input[type=range],
.wbform .field.active input[type=search],
.wbform .field.active input[type=tel],
.wbform .field.active input[type=time],
.wbform .field.active input[type=url],
.wbform .field.active input[type=week],
.wbform .field.active select,
.wbform .field.active textarea {
  border-color: rgba(0, 0, 0, 0.25);
  box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.25);
}
.wbform .field ul {
  display: inline-block;
  list-style: none;
  padding: 0;
  margin: 0;
}
.wbform .field ul li {
  padding: 0;
  margin: 0;
}
.wbform .field ul li label {
  padding-left: 5px;
  padding-right: 5px;
}
.wbform .field ul li input[type=radio],
.wbform .field ul li input[type=checkbox] {
  min-height: 28px;
  padding: 0;
  margin: 0;
}
.wbform .field input + .field_alert,
.wbform .field select + .field_alert,
.wbform .field textarea + .field_alert {
  margin-left: 5px;
}
