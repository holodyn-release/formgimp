// Trim Functions
// ----------------------------------------------------
if( typeof trim == 'undefined' ){
  var trim = function(stringToTrim) {
    if(!stringToTrim)return '';
    return stringToTrim.replace(/^\s+|\s+$/g,"");
  }
}
if( typeof ltrim == 'undefined' ){
  var ltrim = function(stringToTrim) {
    if(!stringToTrim)return '';
    return stringToTrim.replace(/^\s+/,"");
  }
}
if( typeof rtrim == 'undefined' ){
  var rtrim = function(stringToTrim) {
    if(!stringToTrim)return '';
    return stringToTrim.replace(/\s+$/,"");
  }
}

// Checkall
if( typeof checkAll == 'undefined' ){
  var checkAll = function(el) {
    jQuery(el)
      .closest('form')
      .find('input[name^=cid][type=checkbox]')
      .attr('checked', jQuery(el).is(':checked'));
  }
}

// Validate & Submit
// ----------------------------------------------------
if( typeof wbPack == 'undefined' || typeof hideMainMenu == 'undefined' ){
  var hideMainMenu = function(){};
}
if( typeof wbPack == 'undefined' || typeof submitbutton == 'undefined' ){
  var submitbutton = function(task){submitForm(task);}
}
if( typeof wbPack == 'undefined' || typeof submitForm == 'undefined' ){
  var submitForm = function(task,skipCheck){
    var formName = 'adminForm';
    if(task&&task.split(/\|/).length==2){
      formName = task.split(/\|/)[0];
      task = task.split(/\|/)[1];
      if( formName == 'call' ){
        eval( task );
        return false;
      }
    }
    var form = document.getElementById(formName);
    if(!task)task = form.task.value;
    if( task.match(/delete$/) != null )
      if(!confirm('Are you sure you wish to delete this Record?'))
        return false;
    /*
    formValidate = [
      ['fieldName', /match/, 'Message']
      ]
     */
    if( task.match(/save|apply/) != null && skipCheck != true && typeof formValidate == 'object' ){
      for(var i=0;i < formValidate.length;i++){
        if( form[ formValidate[i][0] ] ){
          var check = trim(form[ formValidate[i][0] ].value);
          if( form[ formValidate[i][0] ].type == 'checkbox' )
            if( !form[ formValidate[i][0] ].checked )
              check = '';
          if( form[ formValidate[i][0] ].type == 'radio' )
            if( !form[ formValidate[i][0] ].checked )
              check = '';
          if( check.match(formValidate[i][1]) == null ) {
            alert(formValidate[i][2]);
            return false;
          }
        }
      }
    }
    form.task.value=task;
    form.submit();
    return false;
  }
}

// Load / Redirect Page
// ----------------------------------------------------
if( typeof loadPage == 'undefined' ){
  var loadPage = function( url ){
    if( url != '' )document.location=url;
  }
}

// New Window
// ----------------------------------------------------
if( typeof newWindow == 'undefined' ){
  var newWindow = function(url, width, height) {
    if( url.length && width && height )
      window.open( url, 'new_window', 'scrollbars=1,width='+width+',height='+height );
    if( url.length )
      window.open( url );
    return false;
  }
}

// Clean Number
// ----------------------------------------------------
if( typeof cleanNumber == 'undefined' ){
  var cleanNumber = function( input ) {
    var myChar, i, b, temp = ''; input = String( input );
    var Nums = new Array('0','1','2','3','4','5','6','7','8','9','.');
    for (var i=0; i < input.length; i++) {
      myChar = input.substring(i,i+1);
      for (var b=0; b < Nums.length; b++)
        if (myChar == Nums[b]) temp += myChar;
    }
    return temp;
  }

// Validate Email Address
// ----------------------------------------------------
if( typeof checkEmail == 'undefined' )
  var checkEmail = function( myObj ){
    var str=trim(myObj.value);
    var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (filter.test(str)) {
      if( document.getElementById('formGimp_form').website_url )
        if( trim(document.getElementById('formGimp_form').website_url.value) == '' ){
          document.getElementById('formGimp_form').website_url.value = myObj.value.replace( /^.*\@/, '' );
          formatURL( document.getElementById('formGimp_form').website_url );
        }
    } else {
      alert("Please input a valid email address!");
      myObj.focus();
    }
    return;
  };
}

// Format Phone Field
// ----------------------------------------------------
if( typeof formatPhone == 'undefined' ){
  var formatPhone = function( myObj ) {
    var temp = myObj.value.replace(/\-|\s|\.|\(|\)/g,'');
    if( temp.match( /\d{10}/ ) ) {
      var NewVal = ''; var c = 0;
      for (var i=0; i < temp.length; i++) {
        if (i == 0) { NewVal = "("; };
        if (i == 3) { NewVal = NewVal + ") "; };
        if (i == 6) { NewVal = NewVal + "-"; };
        if (i == 10) { NewVal = NewVal + " "; };
        NewVal = NewVal + temp.substring(i,i+1);
      };
      myObj.focus();
      myObj.value = NewVal;
    }
    return true;
  };
}

// Format URL Field
// ----------------------------------------------------
if( typeof formatURL == 'undefined' ){
  var formatURL = function( myObj ){
    var value = trim(myObj.value);
    if( value.length ){
      if( value.match( /^http|ftp/ ) == null )
        if( value.match( /^\w+\.\w+\/{0,1}$/ ) != null )
          value = 'www.'+value;
      if( value.match( /^http|ftp/ ) == null )
        value = 'http://'+value;
      if( value.match( /\.com$|\.org$|\.net$|\.us$/ ) != null )
        value = value+'/';
    }
    myObj.value = value;
  }
}

// Format Date Field
// ----------------------------------------------------
if( typeof formatDate == 'undefined' ){
  var formatDate = function( myObj, format ){
    var value = trim(myObj.value);
    if( value.length ){
      if( value.match( /^\d{4,6}/ ) != null ){ //mmddyyyy
        if( value.length > 6 )
          value = value.replace( /(\d{2})(\d{2})(\d{4})/, "$3-$1-$2" );
        else
          value = value.replace( /(\d{2})(\d{2})(\d{2})/, "20$3-$1-$2" );
      } else if( value.match( /\d{2}.\d{2}.\d{4}/ ) != null ){ //mm-dd-yyyy
        value = value.replace( /(\d{2}).(\d{2}).(\d{4})/, "$3-$1-$2" );
      } else if( value.match( /\d{1,2}.\d{1,2}.\d{4}/ ) != null ){ //m-d-yyyy
        value = value.replace( /(\d{1,2}).(\d{1,2}).(\d{4})/, "$3-$1-$2" );
      } else if( value.match( /\d{1,2}.\d{1,2}.\d{2}/ ) != null ){ //m-d-yy
        value = value.replace( /(\d{1,2}).(\d{1,2}).(\d{2})/, "20$3-$1-$2" );
      } else {
        alert("Please Correct the Date Format:\n  yyyy-mm-dd\n  mm-dd-yyyy\n  m-d-yy");
        myObj.focus();
      }
    }
    myObj.value = value;
  }
}

// Format Price
// ----------------------------------------------------
if( typeof formatPrice == 'undefined' ){
  var formatPrice = function( myObj, format ) {
    var temp = cleanNumber( myObj.value );
    var pre = temp; var post = "00";
    for (var i=(temp.length-1); i >= 0; i=i-1) {
      if (temp.substring(i,i+1) == '.') {
        pre = temp.substring(0,i);
        if (i > (temp.length-3))
          post = temp.substring(i+1,temp.length) + "0";
        else
          post = temp.substring(i+1,i+3);
      }
    }
    var NewVal = ''; var c = 0;
    for (var i=(pre.length-1); i >= 0; i=i-1) {
      if (c > 2) { NewVal = "," + NewVal; c = 0; };
      NewVal = pre.substring(i,i+1) + NewVal; c++;
    }
    if( format == 1 ){
      NewVal = "$" + NewVal + "." + post;
    } else if( format == 1 ){
      NewVal = NewVal + ( post > 0 ? "." + post : '' );
    } else if( format == 2 ){
      NewVal = NewVal + "." + post;
    } else {
      NewVal = "$" + NewVal + ( post > 0 ? "." + post : '' );
    }
    myObj.value = NewVal;
  }
}

//Get Select List Box Text
//----------------------------------------------------
if( typeof getSelectedIndexText == 'undefined' ){
  var getSelectedIndexText = function( form_id, field_id ) {
    return document.forms[ form_id ][ field_id ].options[ document.forms[ form_id ][ field_id ].selectedIndex ].text;
  }
}

//Get Select List Box Value
//----------------------------------------------------
if( typeof getSelectedIndexValue == 'undefined' ){
  var getSelectedIndexValue = function( form_id, field_id ) {
    return document.forms[ form_id ][ field_id ].options[ document.forms[ form_id ][ field_id ].selectedIndex ].value;
  }
}

//Filter Select List Box
//----------------------------------------------------
var filterSelectListOpt = Array();
var filterSelectListBox = Array();
if( typeof filterSelectList == 'undefined' ){
  var filterSelectList = function( myObj, filter_str ){
    if( filterSelectListOpt[ myObj.id ] == null )
      filterSelectListOpt[ myObj.id ] = myObj.options[ myObj.selectedIndex ].value;
    if( !filterSelectListBox[ myObj.id ] ){
      filterSelectListBox[ myObj.id ] = Array();
      for( var i=0; i < myObj.options.length; i++ )
        filterSelectListBox[ myObj.id ][i] = myObj.options[i];
    }
    while( myObj.options.length ) myObj.options[0] = null;
    var newSelectList = Array(), sIndex = 0;
    for( var i=0; i < filterSelectListBox[ myObj.id ].length; i++ )
      if( filterSelectListBox[ myObj.id ][ i ].text.match( filter_str ) )
        newSelectList.push( filterSelectListBox[ myObj.id ][ i ] );
    for( var i=0; i < newSelectList.length; i++ ){
      myObj.options[i] = newSelectList[i];
      if( myObj.options[i].value == filterSelectListOpt[ myObj.id ] )
        sIndex = i;
    }
    myObj.options.selectedIndex = sIndex;
  }
}

//Limit Input Length in TextArea Objects
//----------------------------------------------------
if( typeof limitInput == 'undefined' ){
  var limitInput = function(fieldObj,maxChars,reportField,outPut) {
    var result = true;
    if (fieldObj.value.length >= maxChars) {
      fieldObj.value = fieldObj.value.substring(0,maxChars);
      result = false;
    }
    if (reportField != '')
      document.getElementById(reportField).innerHTML = sprintf(outPut,(maxChars - fieldObj.value.length));
    if (window.event) window.event.returnValue = result;
    return result;
  }
}

//Copy Object Contents to Clipboard
//----------------------------------------------------
if( typeof copyToCB == 'undefined' ){
  var copyToCB = function( myObj ){
    myObj.select();
    if(document.all){
      txt=myObj.createTextRange();
      txt.execCommand("Copy");
    } else {
      alert('Right Click on the Highlighted Text and Select COPY');
    }
  }
}

//This code is in the public domain.
//Feel free to link back to http://jan.moesen.nu/
//source: http://jan.moesen.nu/code/javascript/sprintf-and-printf-in-javascript/
//----------------------------------------------------
if( typeof sprintf == 'undefined' ){
  var sprintf = function() {
    if (!arguments || arguments.length < 1 || !RegExp) return;
    var str = arguments[0];
    var re = /([^%]*)%('.|0|\x20)?(-)?(\d+)?(\.\d+)?(%|b|c|d|u|f|o|s|x|X)(.*)/;
    var a = b = [], numSubstitutions = 0, numMatches = 0;
    while (a = re.exec(str)) {
      var leftpart = a[1], pPad = a[2], pJustify = a[3], pMinLength = a[4];
      var pPrecision = a[5], pType = a[6], rightPart = a[7];
      numMatches++;
      if (pType == '%'){
        subst = '%';
      } else {
        numSubstitutions++;
        if (numSubstitutions >= arguments.length)
        {
          alert('Error! Not enough var arguments  = function(' + (arguments.length - 1) + ', excluding the string)\nfor the number of substitution parameters in string (' + numSubstitutions + ' so far).');
        }
        var param = arguments[numSubstitutions];
        var pad = '';
               if (pPad && pPad.substr(0,1) == "'") pad = leftpart.substr(1,1);
          else if (pPad) pad = pPad;
        var justifyRight = true;
               if (pJustify && pJustify === "-") justifyRight = false;
        var minLength = -1;
               if (pMinLength) minLength = parseInt(pMinLength);
        var precision = -1;
               if (pPrecision && pType == 'f') precision = parseInt(pPrecision.substring(1));
        var subst = param;
               if (pType == 'b') subst = parseInt(param).toString(2);
          else if (pType == 'c') subst = String.fromCharCode(parseInt(param));
          else if (pType == 'd') subst = parseInt(param) ? parseInt(param) : 0;
          else if (pType == 'u') subst = Math.abs(param);
          else if (pType == 'f') subst = (precision > -1) ? Math.round(parseFloat(param) * Math.pow(10, precision)) / Math.pow(10, precision): parseFloat(param);
          else if (pType == 'o') subst = parseInt(param).toString(8);
          else if (pType == 's') subst = param;
          else if (pType == 'x') subst = ('' + parseInt(param).toString(16)).toLowerCase();
          else if (pType == 'X') subst = ('' + parseInt(param).toString(16)).toUpperCase();
      }
      str = leftpart + subst + rightPart;
    }
    return str;
  }
}

//Parse URL into Variables
//----------------------------------------------------
if( typeof parseVarStr == 'undefined' ){
  var parseVarStr = function( varStr ) {
    if (varStr.length == 0)
      return;
    var nPos = 0;
    var sChar = "";
    var sWord = "";
    var nMaxVars = 0;
    var sVarLine = "";
    var sName = new Array();
    var sValue = new Array();
  //Parse
    while (nPos < varStr.length) {
      sChar = varStr.substring(nPos, nPos + 1);
      if (sChar == "=") {
        sName[nMaxVars] = sWord;
        sWord = "";
      } else if (sChar == "&") {
        sValue[nMaxVars] = sWord;
        sWord = "";
        nMaxVars++;
      } else if (nPos == varStr.length - 1) {
        sWord += varStr.substring(nPos, nPos + 1);
        sValue[nMaxVars] = sWord;
        sWord = "";
        nMaxVars++;
      } else {
        sWord += sChar;
      }
      nPos++;
    }
    return new Array( sName, sValue );
  }
}

// formGimp (need to migrate all above)
//----------------------------------------------------

var formGimp = {
  forms: {
    edit: {
      delField: function(el){
        var tr = el.parentNode.parentNode;
        tr.parentNode.removeChild(tr);
        return false;
      },
      newField: function(){
        var table       = document.getElementById('wbsortable');
        var tableBody   = table.tBodies[0];
        var lastField   = tableBody.getElementsByTagName('TR')[ tableBody.getElementsByTagName('TR').length - 1 ];
        var inputFields = lastField.getElementsByTagName('INPUT');
        for(var i=0;i<inputFields.length;i++) if(inputFields[i].name.match(/name/)) lastName = inputFields[i];
        if( table && lastName.value ){
          var fieldKey = 'field'+fieldCount;
          var docBody = document.getElementsByTagName('body').item(0);
          var newRow  = document.createElement('TR'); newRow.id = 'field'+fieldCount;
          var newCell = document.createElement('TD');
            newCell.setAttribute('align','center');
            newCell.innerHTML
              = '<div><input type="checkbox" name="'+fieldKey+'[visible]" value="1" checked /></div>';
            newRow.appendChild(newCell);
          var newCell = document.createElement('TD');
            newCell.innerHTML
              = '<div class="field-select"><select name="'+fieldKey+'[type]">'
              + '<optgroup label="Standard">'
              + '<option value="text">Input Field</option>'
              + '<option value="textarea">Textarea Field</option>'
              + '<option value="list">Select List</option>'
              + '<option value="radio">Radio List</option>'
              + '<option value="checkbox">Checkbox</option>'
              + '</optgroup>'
              + '<optgroup label="Extended">'
              + '<option value="label">Text Block</option>'
              + '<option value="file">File Upload</option>'
              + '<option value="hidden">Hidden Value</option>'
              + '<option value="number">Number Field</option>'
              + '<option value="password">Password Field</option>'
              + '<option value="date">Date Field</option>'
              + '<option value="time">Time Field</option>'
              + '<option value="datetime-local">Date/Time Field</option>'
              + '</optgroup>'
              + '</select></div>'
              + '<div class="field-text"><input type="text" name="'+fieldKey+'[name]" value="" /></div>';
            newRow.appendChild(newCell);
          var newCell = document.createElement('TD');
            newCell.innerHTML
              = '<div class="field-text"><input type="text" name="'+fieldKey+'[label]" value="" /></div>'
              + '<div class="field-text"><input type="text" name="'+fieldKey+'[tooltip]" value="" /></div>';
            newRow.appendChild(newCell);
          var newCell = document.createElement('TD');
            newCell.innerHTML
              = '<div class="field-textarea"><textarea name="'+fieldKey+'[options]"></textarea></div>';
            newRow.appendChild(newCell);
          var newCell = document.createElement('TD');
            newCell.innerHTML
              = '<div class="field-checkbox"><input type="checkbox" name="'+fieldKey+'[required]" value="1" checked /></div>'
              + '<div class="field-text"><input type="text" name="'+fieldKey+'[validate]" value="\\w+" /></div>';
            newRow.appendChild(newCell);
          var newCell = document.createElement('TD'); newCell.className = 'control';
            newCell.innerHTML
              = '<button type="button" class="btn btn-sm btn-danger" onclick="return formGimp.forms.edit.delField(this);"> Delete </button>';
            newRow.appendChild(newCell);
          lastField.parentNode.insertBefore(newRow,lastField.nextSibling);
          fieldCount++;
        } else {
          lastName.style.border='2px solid #FF0000';
          lastName.onclick=function(){ this.style.border='1px solid #999999'; };
        }
        if( typeof(mySortable) != 'undefined' )
          mySortable._init();
        return false;
      }
    }
  }
}

jQuery(document).ready(function($){
  if (typeof $.fn.tooltip == 'function')
    $('.formgimp .tooltip').tooltip();
});