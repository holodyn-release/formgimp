# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Change Tags

- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

## 1.3.0 - 2021-10-20
### Added
- submission public view controller
- thumbnail media loader controller

## 1.2.0 - 2021-08-27
### Added
- App::attachPath() method
- attach_path configuration definition
- jpeg format to thumbnail filter
- aes_encrypt / decrypt for shared links
- improved pagination display

### Fixed
- broken pagination behavior

## 1.1.1 - 2020-02-19
### Added
- data tags to the form/submission list view data tables

## 1.1.0 - 2020-02-19
### Added
- Language callback support
- Better session existance detection
- Sanitation to form div wrapper classname generation
- Added support for number, password, date, time, and datetime field types
- Confirmation message assignment per-form
- Publish Start/Stop date feature
- Maximum submission limit feature
- Duplicate email submission prevention feature
- Processor configuration json object assignment

### Changed
- Toolbar layout in administration
- Button sizes throughout admin (removed btn-sm)
- Replaced footer in administration with simple text copyright message

### Fixed
- Form rendering support for selected values in select, radio, and checkbox fields

### Removed
- Embedded WP get_template_directory call