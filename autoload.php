<?php

spl_autoload_register(function ($class) {

  /**
   * Validate
   */
  if (strpos($class, 'WebuddhaInc\\FormGimp\\') !== 0) {
    return;
  }

  /**
   * Lookup
   */
  $class = str_replace('\\', '/', substr($class, strlen('WebuddhaInc\\FormGimp\\')));
  $path  = dirname(__FILE__).'/src/'.$class.'.php';
  if (is_readable($path)) {
    require_once $path;
  }

});
